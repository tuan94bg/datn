﻿using UnityEngine;
using System.Collections;
using CnControls;

public class CameraControl : MonoBehaviour
{
	#region PUBLIC_MEMBER

	public GameObject player;

    public float viewPortLimitLeft;
    public float viewPortLimitTop;
    public float viewPortLimitRight;
    public float viewPortLimitBottom;
	#endregion

	#region PRIVATE_MEMBER

	#endregion

	#region UNITY_MONOBEHAVIOUR_METHOD

	void Awake()
	{
		//player = GameObject.FindGameObjectWithTag (Tags.player);
        transform.position = Vector3.zero;
	}

	void LateUpdate ()
	{
        Vector3 tar = player.transform.position;
        float ver = CnInputManager.GetAxis("Vertical") *0.5f;
        if (ver < 0) ver = 0;

        tar.x += CnInputManager.GetAxis("Horizontal") * 0.5f;
        tar.y += ver;

        tar.x = Mathf.Clamp(tar.x, viewPortLimitLeft, viewPortLimitRight);
        tar.y = Mathf.Clamp(tar.y, viewPortLimitBottom, viewPortLimitTop);
        tar.z=-10f;
        Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, tar, 2f * Time.deltaTime);
	}

	#endregion

	#region MY_METHOD

	#endregion
}
