﻿using UnityEngine;
using System.Collections;

public class PointSpawn : MonoBehaviour
{
    public ENEMY_ID id = ENEMY_ID.RUNNER;
    private float lastTimeShowEnemy = -5;
    private Enemy lastEnemy;
    void Start()
    {
        int maxLevelOfEnemy=0;
        switch (GameInit.levelIndex)
        {
            case 1:
                maxLevelOfEnemy = 4;
                break;
            case 2 :
                maxLevelOfEnemy = 6;
                break;
            default :
                maxLevelOfEnemy = 6;
                break;
        }

        id = (ENEMY_ID)Random.Range(1,maxLevelOfEnemy+1);
    }
    void OnBecameInvisible()
    {
        //		Debug.Log(gameObject.name + "out");
    }

    void OnBecameVisible()
    {
        //Debug.Log(gameObject.name+" in");
        if (Time.time - lastTimeShowEnemy > 5 || (lastEnemy != null && lastEnemy.health > 0 && !lastEnemy.gameObject.activeInHierarchy))
        {
            lastTimeShowEnemy = Time.time;
            Enemy enemy = Poolling.Instance.GetEnemyFromPool(id);
            enemy.transform.position = this.transform.position;
            enemy.ClearForVisible();
            enemy.gameObject.SetActive(true);
            lastEnemy = enemy;
        }
    }
}
