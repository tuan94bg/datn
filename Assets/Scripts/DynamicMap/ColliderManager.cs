﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class ColliderManager : MonoBehaviour
{
    //public GameObject haveCollider;
    public SpriteRenderer renderer;
    public AutogenPointSpawn genEnemyController;
    public GameObject horizontal;
    public GameObject vertical;
    public GameObject endMapPoint;
    public Transform limitPointLeft;
    public Transform limitPointTop;
    public Transform limitPointRight;
    public Transform limitPointBottom;


    public List<int> mapType;
    public int bossId;
    public float bossPosX;
    public float bossPosY;
    public int timer = -1;


    public MapData stageData = new MapData();
    //void Start()
    //{
    //    //Test();
    //}

    public void AutoGenEnemy()
    {
        if (stageData.typeMap.Contains((int)MapRequired.MORMAL))
        {
            genEnemyController.AutoGen();
        }
        else if (stageData.typeMap.Contains((int)MapRequired.TIMER))
        {
            genEnemyController.AutoGen();
        }
        else if (stageData.typeMap.Contains((int)MapRequired.BOSS))
        {
            GameController.Instance.menuController.ShowHealthBossMax(10);
            genEnemyController.GenBoss(new Vector3(stageData.requiredData.bossPosition.x, stageData.requiredData.bossPosition.x, 0f));
        }
    }
    void Awake()
    {
        //MapData all = new MapData();
        //string data = ((TextAsset)Resources.Load("MapData/Stage1/Stage1") as TextAsset).text;




        renderer.sprite = GameInit.mapImageSpriteData;
        GenColliderFromJson(GameInit.mapColliderData);
        if (GameInit.isRandomEnemy)
        {
            AutoGenEnemy();
        }
        


        //StartCoroutine(StartDownLoad());
        //Sprite sprite = renderer.sprite;

        //byte[] data = sprite.texture.GetRawTextureData();
        //string response = string.Empty;

        //foreach (byte b in data)
        //    response += (char)b;

        //Debug.Log(response);
        //Debug.Log(ConvertToJson());
    }

    void Test()
    {
        if (mapType.Contains((int)MapRequired.BOSS))
        {
            GameController.Instance.cameraControl.player = this.gameObject;
        }
        CameraControl camControl = GameController.Instance.cameraControl;

        //limitPointLeft.position = stageData.limitPointLeft.ToVector2();
        //limitPointTop.position = stageData.limitPointTop.ToVector2();
        //limitPointRight.position = stageData.limitPointRight.ToVector2();
        //limitPointBottom.position = stageData.limitPointBottom.ToVector2();


        Vector3 limit = Camera.main.ViewportToWorldPoint(Vector3.zero);
        Debug.Log(limit);
        camControl.viewPortLimitLeft = limitPointLeft.position.x + Mathf.Abs(limit.x - Camera.main.transform.position.x);
        camControl.viewPortLimitRight = limitPointRight.position.x - Mathf.Abs(limit.x - Camera.main.transform.position.x);
        camControl.viewPortLimitTop = limitPointTop.position.y - Mathf.Abs(limit.y - Camera.main.transform.position.y);
        camControl.viewPortLimitBottom = limitPointBottom.position.y + Mathf.Abs(limit.y - Camera.main.transform.position.y);
    }

    public void GenJson()
    {
        Debug.Log(ConvertToJson());
    }

    public string ConvertToJson()
    {
        BoxCollider2D[] boxCollidersHorizontal = horizontal.GetComponents<BoxCollider2D>();
        BoxCollider2D[] boxCollidersVertical = vertical.GetComponents<BoxCollider2D>();

        MapData allCollider = new MapData();

        allCollider.typeMap = mapType;
        foreach (BoxCollider2D col in boxCollidersHorizontal)
        {
            PerCollider percol = new PerCollider();
            percol.offset.x = col.gameObject.transform.position.x + col.offset.x;
            percol.offset.y = col.gameObject.transform.position.y + col.offset.y;
            percol.size.x = col.size.x;
            percol.size.y = col.size.y;
            allCollider.colliderHorizontal.Add(percol);
        }
        foreach (BoxCollider2D col in boxCollidersVertical)
        {
            PerCollider percol = new PerCollider();
            percol.offset.x = col.gameObject.transform.position.x + col.offset.x;
            percol.offset.y = col.gameObject.transform.position.y + col.offset.y;
            percol.size.x = col.size.x;
            percol.size.y = col.size.y;
            allCollider.colliderVertical.Add(percol);
        }
        allCollider.requiredData.endMap.offset.x = endMapPoint.transform.position.x;
        allCollider.requiredData.endMap.offset.y = endMapPoint.transform.position.y;
        if (endMapPoint.GetComponent<BoxCollider2D>() != null)
        {
            allCollider.requiredData.endMap.size.x = endMapPoint.GetComponent<BoxCollider2D>().size.x;
            allCollider.requiredData.endMap.size.y = endMapPoint.GetComponent<BoxCollider2D>().size.y;
        }
        else
        {
            allCollider.requiredData.endMap.size.x = 0;
            allCollider.requiredData.endMap.size.y = 0;
        }

        allCollider.requiredData.bossID = bossId;
        allCollider.requiredData.bossPosition.x = bossPosX;
        allCollider.requiredData.bossPosition.y = bossPosY;
        allCollider.requiredData.limitTime = timer;


        allCollider.limitPointLeft.SetValue(limitPointLeft.transform.position);
        allCollider.limitPointTop.SetValue(limitPointTop.transform.position);
        allCollider.limitPointRight.SetValue(limitPointRight.transform.position);
        allCollider.limitPointBottom.SetValue(limitPointBottom.transform.position);
        string result = JsonConvert.SerializeObject(allCollider);
        return result;
    }

    public void GenColliderFromJson(string jsonData)
    {
        stageData = JsonConvert.DeserializeObject<MapData>(jsonData);

        //Horizontal
        foreach (PerCollider col in stageData.colliderHorizontal)
        {
            BoxCollider2D Box = (BoxCollider2D)horizontal.gameObject.AddComponent<BoxCollider2D>();
            Box.offset = col.offset.ToVector2();
            Box.size = col.size.ToVector2();
        }

        // Vertical
        foreach (PerCollider col in stageData.colliderVertical)
        {
            BoxCollider2D Box = (BoxCollider2D)vertical.gameObject.AddComponent<BoxCollider2D>();
            Box.offset = col.offset.ToVector2();
            Box.size = col.size.ToVector2();
        }

        if (stageData.typeMap.Contains((int)MapRequired.MORMAL))
        {
            // End Map
            endMapPoint.transform.position = stageData.requiredData.endMap.offset.ToVector2();
            BoxCollider2D endMapbox = (BoxCollider2D)endMapPoint.gameObject.AddComponent<BoxCollider2D>();
            endMapbox.isTrigger = true;
            endMapbox.size = stageData.requiredData.endMap.size.ToVector2();
        }


        CameraControl camControl = GameController.Instance.cameraControl;

        limitPointLeft.position = stageData.limitPointLeft.ToVector2();
        limitPointTop.position = stageData.limitPointTop.ToVector2();
        limitPointRight.position = stageData.limitPointRight.ToVector2();
        limitPointBottom.position = stageData.limitPointBottom.ToVector2();


        Vector3 limit = Camera.main.ViewportToWorldPoint(Vector3.zero);
        Debug.Log(limit);
        camControl.viewPortLimitLeft = limitPointLeft.position.x + Mathf.Abs(limit.x - Camera.main.transform.position.x);
        camControl.viewPortLimitRight = limitPointRight.position.x - Mathf.Abs(limit.x - Camera.main.transform.position.x);
        camControl.viewPortLimitTop = limitPointTop.position.y - Mathf.Abs(limit.y - Camera.main.transform.position.y);
        camControl.viewPortLimitBottom = limitPointBottom.position.y + Mathf.Abs(limit.y - Camera.main.transform.position.y);

    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        //Top
        Gizmos.DrawLine(new Vector3(limitPointLeft.position.x, limitPointTop.position.y, 0), new Vector3(limitPointRight.position.x, limitPointTop.position.y, 0));
        //Bottom
        Gizmos.DrawLine(new Vector3(limitPointLeft.position.x, limitPointBottom.position.y, 0), new Vector3(limitPointRight.position.x, limitPointBottom.position.y, 0));
        //Left
        Gizmos.DrawLine(new Vector3(limitPointLeft.position.x, limitPointTop.position.y, 0), new Vector3(limitPointLeft.position.x, limitPointBottom.position.y, 0));
        //Right
        Gizmos.DrawLine(new Vector3(limitPointRight.position.x, limitPointTop.position.y, 0), new Vector3(limitPointRight.position.x, limitPointBottom.position.y, 0));
    }
#endif
}

public enum MapRequired
{
    MORMAL = 0,
    BOSS = 1,
    TIMER = 2
}

public class MapData
{
    public List<int> typeMap = new List<int>() { (int)MapRequired.BOSS };
    public RequiredData requiredData = new RequiredData();
    public List<PerCollider> colliderHorizontal = new List<PerCollider>();
    public List<PerCollider> colliderVertical = new List<PerCollider>();
    //public PerCollider endMap = new PerCollider();
    public MyVector limitPointLeft = new MyVector();
    public MyVector limitPointTop = new MyVector();
    public MyVector limitPointRight = new MyVector();
    public MyVector limitPointBottom = new MyVector();

}
//public class ColliderList
//{
//    public List<PerCollider> colliderList = new List<PerCollider>();
//}

public class PerCollider
{
    public MyVector offset = new MyVector();
    public MyVector size = new MyVector();
}

public class RequiredData
{
    public int bossID = -1;
    public MyVector bossPosition = new MyVector();
    public int limitTime = -1;
    public PerCollider endMap = new PerCollider();
}

public class MyVector
{
    public float x;
    public float y;

    public Vector2 ToVector2()
    {
        Vector2 result = new Vector2(this.x, this.y);
        return result;
    }
    public void SetValue(Vector3 pos)
    {
        this.x = pos.x;
        this.y = pos.y;
        Debug.Log(pos.x);
    }
}