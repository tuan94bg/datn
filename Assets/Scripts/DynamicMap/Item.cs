﻿using UnityEngine;
using System.Collections;


public enum ItemId
{
	SCORE = 0,
	AMMO = 1,
	HEALTH = 2
}

public class Item : MonoBehaviour {

	public Sprite[] sprites;
	public ItemId id = ItemId.SCORE;
	protected CharacterController player;

	private SpriteRenderer sprite;
	void Awake()
	{
        player = GameController.Instance.playerController;
		sprite = GetComponent<SpriteRenderer>();
        transform.parent = GameController.Instance.poolContainer.transform;
		gameObject.SetActive(false);
	}

	void OnEnable()
	{
		id = (ItemId) Random.Range(0,2);
		sprite.sprite = sprites[(int)id];
        sprite.color = new Color32(255, 255, 255, 255);
		StartCoroutine(Flicker());
	}

	private IEnumerator Flicker()
	{
		yield return new WaitForSeconds(4);
		gameObject.SetActive(false);
	}
	void OnBecameInvisible() {
		Poolling.Instance.ReturnItemPool(this);
	}	

//	void OnBecameVisible()
//	{
//		id = (ItemId) Random.Range(0,2);
//		sprite = sprites[(int)id];
//		StartCoroutine(Flicker());
//	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag(Tags.playerTrigger))
		{
			player.TakeItem(id);
			this.gameObject.SetActive(false);
		}
	}
}
