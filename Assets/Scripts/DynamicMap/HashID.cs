﻿using UnityEngine;
using System.Collections;

public class HashID : MonoBehaviour 
{
	public int speedFloat;
	public int vSpeedFloat;
	public int jumpTrigger;
	public int groundBool;
	public int punchTrigger;
	public int shotTrigger;
	public int skillTrigger;
	public int jumppingBool;
	public int weaponID;
	public int jumpOnWallBool;
	public int sitBool;
	public int sittingBool;
	public int attackTrigger;
	public int isDieBool;
	public int isDieTrigger;

	void Awake()
	{
		speedFloat = Animator.StringToHash("Speed");
		groundBool = Animator.StringToHash("Ground");
		vSpeedFloat = Animator.StringToHash("vSpeed");
		jumpTrigger = Animator.StringToHash("JumpTrigger");
		jumppingBool = Animator.StringToHash("Jumpping");
		weaponID = Animator.StringToHash("Weapon");
		jumpOnWallBool = Animator.StringToHash("JumpOnWall");
		sitBool = Animator.StringToHash("Sit");
		sittingBool = Animator.StringToHash("Sitting");

		attackTrigger = Animator.StringToHash("Attack");
		isDieTrigger = Animator.StringToHash("isDieTrigger");
		isDieBool = Animator.StringToHash("isDie");
	}
}
