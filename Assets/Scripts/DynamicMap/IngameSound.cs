﻿using UnityEngine;
using System.Collections;

public class IngameSound : MonoBehaviour {

    public AudioSource shootSound;
    public AudioSource jumpSound;
    public AudioSource dieSound;
    public AudioSource takeItemSound;
    public AudioSource punchSound;
    public void PlayShoot()
    {
        if (this.shootSound.isPlaying == false)
        {
            this.shootSound.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.shootSound.Play();
        }
    }

    public void PlayJump()
    {
        if (this.jumpSound.isPlaying == false)
        {
            this.jumpSound.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.jumpSound.Play();
        }
    }

    public void PlayDie()
    {
        if (this.dieSound.isPlaying == false)
        {
            this.dieSound.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.dieSound.Play();
        }
    }

    public void PlayTakeItem()
    {
        if (this.takeItemSound.isPlaying == false)
        {
            this.takeItemSound.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.takeItemSound.Play();
        }
    }

    public void PlayPunch()
    {
        if (this.punchSound.isPlaying == false)
        {
            this.punchSound.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.punchSound.Play();
        }
    }
}
