﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Poolling poolController;
    public CameraControl cameraControl;
    public PlayerController playerController;
    public IngameSound soundInGame;

    [HideInInspector]
    public AIController aiController;

    public ColliderManager mapManager;
    public MenuControl menuController;

    public Transform poolContainer;
    public Transform pointSpawnContainer;
  
    public string stage = string.Empty;
    private static int stageID = 1;

    [HideInInspector]
    public int score;

    public bool isEndGame
    {
        get;
        set;
    }
    public bool isWin
    {
        get;
        set;
    }

    private static GameController instance;
    public static GameController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GameController>();
            }
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        isEndGame = false;
        SoundController.Instance.PlayBackgroundStageMusic(GameInit.levelIndex);
        SetStageName();
    }

    public void SetStage(int id)
    {
        stageID = id;
        Application.LoadLevel("Stage");
    }
    public void SetStageName()
    {
        stage = "Stage" + stageID.ToString();
    }

    public void SentLevelComplete()
    {
        GameHub.Instance.userHub.SetLevel(string.Format("{{0}}",CommonFormat.AddContent(CommonTags.LEVEL,GameInit.levelIndex.ToString())),null);
    }
    public void LevelComplete()
    {
        isEndGame = true;
        isWin = true;
        GameData.Instance.LevelComplete();
        StartCoroutine(GameHub.Instance.SetLevelOnDBAsyn());
        menuController.EndGameResult();
    }

    public void GameOver()
    {
        isEndGame = true;
        isWin = false;
        menuController.EndGameResult();
    }

    public void AddScoreKillEnemy(ENEMY_ID id)
    { 
        switch (id)
        {
            case ENEMY_ID.RUNNER :
                score += 100;
                break;
            case ENEMY_ID.ROBOT:
                score += 150;
                break;
            case ENEMY_ID.FIREGUNNER:
                score += 150;
                break;
            case ENEMY_ID.GUNNER:
                score += 150;
                break;
            case ENEMY_ID.BOMB:
                break;
            case ENEMY_ID.FLIER:
                score += 300;
                break;
        }
    }
}
