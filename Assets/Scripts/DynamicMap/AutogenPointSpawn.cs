﻿using UnityEngine;
using System.Collections;

public class AutogenPointSpawn : MonoBehaviour 
{
    public GameObject pointSpawn;
    public int pointSpawnNumber=10;

    private BoxCollider2D[] allCollider;

    public void Start()
    {
        //GetAllCollider();
       // AutoGen();
    }


    private void GetAllCollider()
    {
        allCollider = GetComponents<BoxCollider2D>();
    }

    public void GenBoss(Vector3 position)
    {
        AIController aiBoss = GameController.Instance.poolController.GetRedBatman();
        aiBoss.transform.position = position;
    }
    public void AutoGen()
    {
        GetAllCollider();
        foreach (BoxCollider2D col in allCollider)
        {
            float spaceGen = 1f;
            
            int maxpoint =(int) (col.size.x / spaceGen);
            maxpoint = maxpoint > 10 ? 10 : maxpoint;
            Vector2 origin;
            float partof = col.offset.x/20;
            origin.x = col.offset.x + col.gameObject.transform.position.x - col.size.x/2;
            origin.y = col.offset.y + col.gameObject.transform.position.y+col.size.y/2+0.001f;
            while (maxpoint >= 0 && origin.x < col.offset.x + col.gameObject.transform.position.x+col.size.x/2f)
            {
                RaycastHit2D hit;

                origin.x += partof;

                if (origin.x<3f && origin.y<1.5f)
                {
                    continue;
                }

                hit = Physics2D.Raycast(origin,Vector3.up,0.4f);
                if (!hit)
                {
                    if (UnityEngine.Random.Range(0,2) == 0)
                    {
                        GameObject pointS = Instantiate(pointSpawn, origin, Quaternion.identity) as GameObject;
                        pointS.transform.parent = GameController.Instance.pointSpawnContainer;
                        maxpoint--;
                        origin.x += spaceGen;
                    }
                }
            }
        }
    }
}
