﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class MenuControl : MonoBehaviour
{
    public static MenuControl Instance;

    CharacterController player;
    public List<Image> healthIconList;
    public List<Image> healthBossIconList;
    public Sprite iconHealFull;
    public Sprite iconHealNull;
    public Sprite iconHealBossFull;
    public Sprite iconHealBossNull;
    private bool isDangerous = false;
    public Sprite[] weaponIconList;
    public Sprite[] weaponIconButtonList;
    public Image currentWeapon;
    public Button btnSwap;
    public Text ammoText;
    public Text coinText;
    public GameObject pausePanel;
    public GameObject inputPanel;

    public RectTransform optionPausePanel;

    public StageResult result;
    void Awake()
    {
        ammoText.enabled = false;
        Instance = this;
        ShowCoin(GameData.Instance.userProfile.coin);
        //		ShowHealth(1);
        //		StartCoroutine(test());
        //		currentWeapon.sprite = weaponIconList[0];
    }

    public void ShowCoin(int coin)
    {
        coinText.text = coin.ToString();
    }

    public void ShowHealthBossMax(int maxhealth)
    {
        for (int i = healthBossIconList.Count - 1; i >= 0; i--)
        {
            if (i >= maxhealth)
            {
                healthBossIconList.RemoveAt(i);
            }
            else
            {
                healthBossIconList[i].enabled = true;
            }
        }
    }

    public void ShowHealthBoss(int health)
    {
        for (int i = 0; i < healthBossIconList.Count; i++)
        {
            if (health == 1) StartCoroutine(Flicker());
            else
            {
                isDangerous = false;
            }

            if (i < health)
            {
                healthBossIconList[i].sprite = iconHealBossFull;
                //if (health<2) healthIconList[i].color = new Color(255,0,0);
            }
            else
            {
                healthBossIconList[i].sprite = iconHealBossNull;
            }

        }
    }

    public void ShowHealthMax(int maxhealth)
    {
        for (int i = healthIconList.Count - 1; i >= 0; i--)
        {
            if (i > maxhealth)
            {
                healthIconList[i].enabled = false;
                healthIconList.RemoveAt(i);
            }
        }
    }

    public void ShowHealth(int health)
    {
        for (int i = 0; i < healthIconList.Count; i++)
        {
            if (health == 1) StartCoroutine(Flicker());
            else
            {
                isDangerous = false;
            }

            if (i < health)
            {
                healthIconList[i].sprite = iconHealFull;
                //if (health<2) healthIconList[i].color = new Color(255,0,0);
            }
            else
            {
                healthIconList[i].sprite = iconHealNull;
            }

        }
    }


    public void ShowIconWeapon(int ammo, WEAPON_STATE currentState)
    {
        ammoText.text = ammo.ToString();
        if (ammo == 0)
        {
            currentWeapon.sprite = weaponIconList[0];
            ammoText.enabled = false;
        }
        else
        {
            int stateInt = (int)currentState;
            currentWeapon.sprite = weaponIconList[stateInt + 1];
            ammoText.enabled = currentState > 0;
            btnSwap.image.sprite = weaponIconButtonList[(stateInt + 1 > 3 ? 0 : stateInt + 1)];
            if (GameController.Instance.playerController.playerState.bulletNumber < stateInt + 1)
            {
                btnSwap.image.sprite = weaponIconButtonList[0];
            }
            //switch (currentState)
            //{
            //case WEAPON_STATE.PUNCH :
            //    currentWeapon.sprite = weaponIconList[1];
            //    ammoText.enabled = false;
            //    btnSwap.image.sprite = weaponIconButtonList[1];
            //    break;
            //case WEAPON_STATE.BOMERANG :
            //    currentWeapon.sprite = weaponIconList[2];
            //    btnSwap.image.sprite = weaponIconButtonList[2];
            //    ammoText.enabled = true;
            //    break;
            //case WEAPON_STATE.GUN :
            //    currentWeapon.sprite = weaponIconList[3];
            //    btnSwap.image.sprite = weaponIconButtonList[3];
            //    ammoText.enabled = true;
            //    break;
            //case WEAPON_STATE.SHURIKEN :
            //    currentWeapon.sprite = weaponIconList[4];
            //    btnSwap.image.sprite = weaponIconButtonList[0];
            //    ammoText.enabled = true;
            //    break;
            //}
        }
    }

    IEnumerator Flicker()
    {
        isDangerous = true;
        while (isDangerous)
        {
            healthIconList[0].sprite = iconHealFull;
            yield return new WaitForSeconds(0.2f);
            if (isDangerous)
            {
                healthIconList[0].sprite = iconHealNull;
                yield return new WaitForSeconds(0.2f);
            }
        }
        healthIconList[0].sprite = iconHealFull;
    }

    public void PauseOrResume()
    {
        if (pausePanel.activeInHierarchy)
        {
            ResumeOnClick();
        }
        else
        {
            PauseOnClick();
        }
    }

    public void PauseOnClick()
    {
        pausePanel.SetActive(true);
        inputPanel.SetActive(false);
        LeanTween.cancelAll();
        LeanTween.moveX(optionPausePanel, 0, 0.5f).setIgnoreTimeScale(true).setEase(LeanTweenType.easeOutCirc);
        Time.timeScale = 0;
    }

    public void ResumeOnClick()
    {
        //pausePanel.SetActive(false);
        LeanTween.moveX(optionPausePanel, optionPausePanel.rect.width, 0.5f).setIgnoreTimeScale(true).setEase(LeanTweenType.easeInCirc);
        Time.timeScale = 1;
        StartCoroutine(wait());
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.5f);
        pausePanel.SetActive(false);
        inputPanel.SetActive(true);
    }
    public void QuitOnClick()
    {
        Time.timeScale = 1;
        GameData.Instance.ResetForNewStage();
        UnityEngine.SceneManagement.SceneManager.LoadScene("World");
    }

    public void PauseGame(bool isPause)
    {
        Time.timeScale = isPause ? 0 : 1;
    }

    public void EndGameResult()
    {
        result.gameObject.SetActive(true);
    }
    //	IEnumerator test()
    //	{
    //		yield return new WaitForSeconds(2.23f);
    //		ShowHealth(4);
    //		ShowIconWeapon(3,WEAPON_STATE.SKILL1);
    //	}
}
