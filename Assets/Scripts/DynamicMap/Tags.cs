﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour 
{
	public const string player = "Player";
	public const string playerTrigger = "PlayerTrigger";
	public const string enemy = "Enemy";
	public const string enemy_bullet = "Enemy_Bullet";
	public const string item = "Item";
	public const string bullet = "Bullet";
	public const string bulletRocket = "Bullet_Rocket";
	public const string gameController = "GameController";
	public const string background = "Background";
	public const string MAP_VER = "MapVertical";
	public const string MAP = "Map";
}
