﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapFormat
{
	[SerializeField] public string mapLink = "";
	[SerializeField] public List<ColliderFormat> listColliderVertical = new List<ColliderFormat>();
//	[SerializeField] public List<ColliderFormat> listColliderHorizontal = new List<ColliderFormat>();
}
