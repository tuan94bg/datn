﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColliderFormat{
	[SerializeField] public bool canRespawn;
	[SerializeField] public string tag;
	[SerializeField] public List<PointFormat> listPoint = new List<PointFormat>();

}
