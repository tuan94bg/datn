﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Poolling : MonoBehaviour
{


    public static Poolling Instance;

    private int enemyID = 0;
    public int EnemyID
    {
        get 
        {
            return enemyID++;
        }
    }
    private int bulletIdName = 0;
    public int BulletIdName
    {
        get
        {
            return bulletIdName++;
        }
    }

    public int levelID = 2;
    //public int levelOfEnemy = 2;

    public GameObject rocket;
    public GameObject bomerang;
    public GameObject bomb;

    public GameObject enemyRunner;
    public GameObject enemyRobot;
    public GameObject enemyFireGunner;
    public GameObject enemyGunner;
    public GameObject enemyBomb;
    public GameObject enemyFlier;
    public GameObject item;

    public GameObject RedBatmanBoss;

    private ObjectPooling<Bullet> rocketPooling;
    private ObjectPooling<Bullet> bomerangPooling;
    private ObjectPooling<Bullet> bombPooling;
    private ObjectPooling<Enemy> enemyRunnerPooling;
    private ObjectPooling<Enemy> enemyFireGunnerPooling;
    private ObjectPooling<Enemy> enemyGunnerPooling;
    private ObjectPooling<Enemy> enemyRobotPooling;
    private ObjectPooling<Enemy> enemyBombPooling;
    private ObjectPooling<Enemy> enemyFlierPooling;

    private ObjectPooling<Item> itemPooling;


    private Dictionary<string, Bullet> bulletInstance = new Dictionary<string, Bullet>();
    private Dictionary<string, Enemy> enemyInstance = new Dictionary<string, Enemy>();
    private Dictionary<string, Item> itemInstance = new Dictionary<string, Item>();

    //private Queue<GameObject> rocketPool = new Queue<GameObject>();
    //private Queue<GameObject> bomerangPool = new Queue<GameObject>();
    //private Queue<GameObject> bombPool = new Queue<GameObject>();

    //private Queue<GameObject> enemyRunnerPool = new Queue<GameObject>();
    //private Queue<GameObject> enemyFireGunnerPool = new Queue<GameObject>();
    //private Queue<GameObject> enemyGunnerPool = new Queue<GameObject>();
    //private Queue<GameObject> enemyRobotPool = new Queue<GameObject>();
    //private Queue<GameObject> enemyBombPool = new Queue<GameObject>();
    //private Queue<GameObject> enemyFlierPool = new Queue<GameObject>();

    private Queue<GameObject> itemPool = new Queue<GameObject>();



    #region Initialization

    public void InitPool()
    {
        rocketPooling = new ObjectPooling<Bullet>(5, null, InitBulletRocket);
        rocketPooling.InitBuffer(5);
        bomerangPooling = new ObjectPooling<Bullet>(5, ResetBomerang, InitBulletBomerang);
        bomerangPooling.InitBuffer(5);
        bombPooling = new ObjectPooling<Bullet>(5, null, InitBulletBomb);
        bombPooling.InitBuffer(5);

        enemyRunnerPooling = new ObjectPooling<Enemy>(5, null, InitEnemyRunner);
        enemyRunnerPooling.InitBuffer(10);
        enemyFireGunnerPooling = new ObjectPooling<Enemy>(5, null, InitEnemyFireGunner);
        enemyFireGunnerPooling.InitBuffer(10);

        enemyGunnerPooling = new ObjectPooling<Enemy>(5, null, InitEnemyGunner);
        enemyGunnerPooling.InitBuffer(10);

        enemyRobotPooling = new ObjectPooling<Enemy>(5, null, InitEnemyRobot);
        enemyRobotPooling.InitBuffer(10);

        enemyBombPooling = new ObjectPooling<Enemy>(5, null, InitEnemyBomb);
        enemyBombPooling.InitBuffer(10);

        enemyFlierPooling = new ObjectPooling<Enemy>(5, null, InitEnemyFlier);
        enemyFlierPooling.InitBuffer(10);

    }

    private void ResetBomerang(Bullet obj)
    {
        obj.transform.rotation = Quaternion.identity;
    }

    #region Init Pool Object

    public void InitBulletRocket(Bullet bul)
    {
        if (bul == null)
        {
            GameObject objPool = Instantiate(rocket, Vector3.zero, Quaternion.identity) as GameObject;
            bul = objPool.GetComponent<Bullet>();
            objPool.SetActive(false);
            rocketPooling.Store(bul);
        }
    }
    public void InitBulletBomerang(Bullet bul)
    {
        if (bul == null)
        {
            GameObject objPool = Instantiate(bomerang, Vector3.zero, Quaternion.identity) as GameObject;
            bul = objPool.GetComponent<Bullet>();
            objPool.SetActive(false);
            bomerangPooling.Store(bul);
        }
    }
    public void InitBulletBomb(Bullet bul)
    {
        if (bul == null)
        {
            GameObject objPool = Instantiate(bomb, Vector3.zero, Quaternion.identity) as GameObject;
            bul = objPool.GetComponent<Bullet>();
            objPool.SetActive(false);
            bombPooling.Store(bul);
        }
    }

    public void InitEnemyRunner(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyRunner, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyRunnerPooling.Store(enemy);
        }
    }
    public void InitEnemyFireGunner(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyFireGunner, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyFireGunnerPooling.Store(enemy);
        }
    }

    public void InitEnemyGunner(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyGunner, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyGunnerPooling.Store(enemy);
        }
    }

    public void InitEnemyRobot(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyRobot, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyRobotPooling.Store(enemy);
        }
    }

    public void InitEnemyBomb(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyBomb, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyBombPooling.Store(enemy);
        }
    }
    public void InitEnemyFlier(Enemy enemy)
    {
        if (enemy == null)
        {
            GameObject objPool = Instantiate(enemyFlier, Vector3.zero, Quaternion.identity) as GameObject;
            enemy = objPool.GetComponent<Enemy>();
            objPool.SetActive(false);
            enemyFlierPooling.Store(enemy);
        }
    }
    #endregion

    void Awake()
    {
        Instance = this;
        //DontDestroyOnLoad(this);
        InitPool();
    }

    #endregion

    #region PlayerBullet
    public Bullet GetPlayerBulletFromPool(WEAPON_STATE state)
    {
        Bullet result = null;
        switch (state)
        {
            case WEAPON_STATE.GUN:
                result = rocketPooling.New();
                break;
            case WEAPON_STATE.BOMERANG:
                result = bomerangPooling.New();
                break;
            case WEAPON_STATE.SHURIKEN:
                result = bombPooling.New();
                break;
        }

        bulletInstance.Add(result.name, result);
        return result;
    }
    public void SaveBulletBackPool(Bullet bullet)
    {
        if (bullet.gameObject.activeInHierarchy)
        {
            switch (bullet.id)
            {
                case BulletID.Rocket:
                    rocketPooling.Store(bullet);
                    break;
                case BulletID.Bomerang:
                    bomerangPooling.Store(bullet);
                    break;
                case BulletID.Bomb:
                    bombPooling.Store(bullet);
                    break;
            }
            bulletInstance.Remove(bullet.name);
            bullet.gameObject.SetActive(false);
        }
    }
    public Bullet GetBulletFromInstanceByName(string name)
    {
        return bulletInstance[name];
    }
    #endregion

    #region Enemy

    public Enemy GetEnemyFromPool(ENEMY_ID id)
    {
        Enemy result = null;
        switch (id)
        {
            case ENEMY_ID.RUNNER:
                result = enemyRunnerPooling.New();
                break;
            case ENEMY_ID.ROBOT:
                result = enemyRunnerPooling.New();
                break;
            case ENEMY_ID.FIREGUNNER:
                result = enemyFireGunnerPooling.New();
                break;
            case ENEMY_ID.GUNNER:
                result = enemyGunnerPooling.New();
                break;
            case ENEMY_ID.BOMB:
                result = enemyBombPooling.New();
                break;
            case ENEMY_ID.FLIER:
                result = enemyFlierPooling.New();
                break;
        }
        enemyInstance.Add(result.name, result);
        return result;
    }
    public void SaveEnemyBackPool(Enemy enemyScript)
    {
        //		Debug.Log("????" + enemyScript.id.ToString());
        switch (enemyScript.id)
        {
            case ENEMY_ID.RUNNER:
                enemyRunnerPooling.Store(enemyScript);
                break;
            case ENEMY_ID.ROBOT:
                enemyRobotPooling.Store(enemyScript);
                break;
            case ENEMY_ID.FIREGUNNER:
                enemyFireGunnerPooling.Store(enemyScript);
                break;
            case ENEMY_ID.GUNNER:
                enemyGunnerPooling.Store(enemyScript);
                break;
            case ENEMY_ID.BOMB:
                enemyBombPooling.Store(enemyScript);
                break;
            case ENEMY_ID.FLIER:
                enemyFlierPooling.Store(enemyScript);
                break;
        }
        enemyInstance.Remove(enemyScript.name);
        enemyScript.gameObject.SetActive(false);
        //		Debug.Log(enemyFireGunnerPool.Count);
    }

    public Enemy GetEnemyFromInstanceByName(string name)
    {
        if (enemyInstance.ContainsKey(name))
        {
            return enemyInstance[name];
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Item

    public void ReturnItemPool(Item itemScript)
    {
        itemScript.gameObject.SetActive(false);
        itemPool.Enqueue(itemScript.gameObject);
    }

    public GameObject GetItem()
    {
        if (itemPool.Count == 0)
        {
            itemPool.Enqueue((GameObject)Instantiate(item) as GameObject);
        }
        return itemPool.Dequeue();
    }

    #endregion

    public AIController GetRedBatman()
    {
        if (GameController.Instance.aiController == null)
        {
            GameObject boss = GameObject.Instantiate(RedBatmanBoss) as GameObject;
            GameController.Instance.aiController = boss.GetComponent<AIController>();
        }
        return GameController.Instance.aiController;
    }

    //	void Update()
    //	{
    //		Debug.Log(enemyRunnerPool.Count);
    //	}
}
