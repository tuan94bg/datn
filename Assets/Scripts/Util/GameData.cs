﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;

public class GameData
{

    private static GameData instance;
    public static GameData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameData();
            }
            return instance;
        }
    }
    public string nameFace;
    public int MaxLevel;
    public int indexWorld = 0;
    public User userProfile = new User();
    public const string defaultStageName = "Stage";
    public int MaxHealth = 10;
    public int startBullet = 0;

    public int hpBuy = 0;
    public int ammoBuy = 0;

    public BonusDay bonusday = new BonusDay();
    public UserSetting setting = new UserSetting();

    private GameData()
    {
        MaxLevel = PlayerPrefs.GetInt("MaxLevel",0);
        nameFace = PlayerPrefs.GetString("nameFace", string.Empty);
        LoadData();
    }

    public void SetNameFacebook(string name)
    {
        nameFace = name;
        PlayerPrefs.SetString("nameFace", nameFace);
    }

    public void SaveMaxLevel(int level)
    {
        MaxLevel = level;
        PlayerPrefs.SetInt("MaxLevel", MaxLevel);
    }

    public void SaveCoin()
    {
        PlayerPrefs.SetInt("user.coin", MaxLevel);
        GameHub.Instance.SetCoin();
    }

    public void LoadData()
    {
        userProfile.id = PlayerPrefs.GetInt("user.id", -1);
        userProfile.idFB = PlayerPrefs.GetString("user.idFB", string.Empty);
        userProfile.name = PlayerPrefs.GetString("user.name", string.Empty);
        userProfile.coin = PlayerPrefs.GetInt("user.coin",0);
        userProfile.level = PlayerPrefs.GetInt("user.level",0);
        userProfile.inviteCode = PlayerPrefs.GetString("user.inviteCode", string.Empty);
        userProfile.isRewardInvite = PlayerPrefs.GetInt("user.isRewardInvite", 0);
        userProfile.inviteRewardTimes = PlayerPrefs.GetInt("user.inviteRewardTimes", 5);

        bonusday.keyDay = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
        bonusday.name = PlayerPrefs.GetString(bonusday.keyDay + ".name", string.Empty);
        bonusday.bonusAmmo = PlayerPrefs.GetInt(bonusday.keyDay + ".bonusAmmo", 0);
        bonusday.bonusHp = PlayerPrefs.GetInt(bonusday.keyDay + ".bonusHp", 0);

        setting.isMuteMusic = PlayerPrefs.GetInt("setting.isMuteMusic");
        setting.isMuteSound = PlayerPrefs.GetInt("setting.isMuteSound");
    }

    public void SaveDataUser()
    {
        PlayerPrefs.SetInt("user.id", userProfile.id);
        PlayerPrefs.SetString("user.idFB", userProfile.idFB);
        PlayerPrefs.SetString("user.name", userProfile.name);
        PlayerPrefs.SetInt("user.coin", userProfile.coin);
        PlayerPrefs.SetInt("user.level", userProfile.level);
        PlayerPrefs.SetString("user.inviteCode", userProfile.inviteCode);
        PlayerPrefs.SetInt("user.isRewardInvite", userProfile.isRewardInvite);
        PlayerPrefs.SetInt("user.inviteRewardTimes", userProfile.inviteRewardTimes);
    }

    public void SaveDataBonus()
    {
        PlayerPrefs.SetString(bonusday.keyDay + ".name",bonusday.name);
        PlayerPrefs.SetInt(bonusday.keyDay + ".bonusAmmo", bonusday.bonusAmmo);
        PlayerPrefs.SetInt(bonusday.keyDay + ".bonusHp", bonusday.bonusHp);
    }

    public void LevelComplete()
    {
        if (GameInit.levelIndex > userProfile.level)
        {
            userProfile.level++;
            PlayerPrefs.SetInt("user.level", userProfile.level);
        }
        
        
    }

    public void SaveSetting()
    {
        PlayerPrefs.SetInt("setting.isMuteMusic", setting.isMuteMusic);
        PlayerPrefs.SetInt("setting.isMuteSound", setting.isMuteSound);
    }

    public static void SaveFileTextOnDevice(string jsonText, string name)
    {
        string path = Path.Combine(Application.persistentDataPath, name + ".txt");
        Debug.Log(path);
        var sr = File.CreateText(path);
        sr.WriteLine(jsonText);
        sr.Close();
    }
    //public static void SaveFileTextOnDevice(byte[] textBytes, string name)
    //{
    //    string content = Encoding.ASCII.GetString(textBytes);
    //    string path = Path.Combine(Application.persistentDataPath, name + ".txt");
    //    Debug.Log(path);
    //    var sr = File.CreateText(path);
    //    sr.WriteLine(content);
    //    sr.Close();
    //}

    public static void SaveFileImageOnDevice(Texture2D imgData, string name)
    {
        string path = Path.Combine(Application.persistentDataPath, name + ".png");
        Debug.Log(path);
        byte[] img = imgData.EncodeToPNG();
        File.WriteAllBytes(path, img);
    }
    public static bool IsStageExistOnResource(int stage)
    {
        string stagename = "Stage" + stage.ToString();
        string path = "MapData/" + stagename + "/" + stagename;
        TextAsset text = Resources.Load(path) as TextAsset;
        if (text != null)
        {
            return true;
        }
        return false;
    }

    public static bool IsStageExistOnDevice(int stage)
    {
        string stagename = "Stage" + stage.ToString();
        string path = Path.Combine(Application.persistentDataPath, stagename);
        return File.Exists(path);
    }
    public static bool IsExistOnLocal(int stage)
    {
        return IsStageExistOnDevice(stage) || IsStageExistOnResource(stage);
    }

    internal void GetBonusCallBack(string arg0)
    {
        Debug.Log(arg0);
        if (arg0 != null)
        {
            Dictionary<string, string> dic = CommonFormat.JsonDataToDictionary(arg0);
            ResponseCode responseCode = CommonFormat.ConvertToResponseCode(dic[CommonTags.RESPONSE_CODE]);
            if (responseCode == ResponseCode.SUCCESS)
            {
                bonusday = JsonConvert.DeserializeObject<BonusDay>(dic[CommonTags.BONUS_DAY]);
                SaveDataBonus();
            }
            else
            {
                
            }
        }
        else
        {
        }
        // TODO : Thong bao khong co mang
    }

    public void ResetForNewStage()
    {
        MaxHealth = 6;
        startBullet = 0;
        hpBuy = 0;
        ammoBuy = 0;
    }
}

public class IntPref
{
    public int value;
    private string key;
    public IntPref(string key,int defaultValue)
    {
        this.key = key;
        value = PlayerPrefs.GetInt(key, defaultValue);
    }
    public void Set(int value)
    {
        if (value != this.value)
        {
            PlayerPrefs.SetInt(key, value);
            this.value = value;
        }
    }
}

public class BonusDay
{
    public string keyDay;
    public string name;
    public int bonusAmmo;
    public int bonusHp;
    public int coinBonus;
}

public class UserSetting
{
    public int isMuteMusic;
    public int isMuteSound;
}