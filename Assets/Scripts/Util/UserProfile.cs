﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class UserProfile
{
    public int id = -1;
    public string idFB = "";
    public string name = "";
    public int coin = 0;
    public int level = 0;
    public string inviteCode = "";
    public int isRewardInvite = 0; // 
    public int inviteRewardTimes = 5;

    public void LoadData()
    {
        try
        {
            id = PlayerPrefs.GetInt("user.id");
            idFB = PlayerPrefs.GetString("user.idFB");
            name = PlayerPrefs.GetString("user.name");
            coin = PlayerPrefs.GetInt("user.coin");
            level = PlayerPrefs.GetInt("user.level");
            inviteCode = PlayerPrefs.GetString("user.inviteCode");
            isRewardInvite = PlayerPrefs.GetInt("user.isRewardInvite");
            inviteRewardTimes = PlayerPrefs.GetInt("user.inviteRewardTimes");
        }
        catch (Exception ex)
        {

        }
    }
    public void SaveData()
    {
        PlayerPrefs.SetInt("user.id", id);
        PlayerPrefs.SetString("user.idFB", idFB);
        PlayerPrefs.SetString("user.name", name);
        PlayerPrefs.SetInt("user.coin", coin);
        PlayerPrefs.SetInt("user.level", level);
        PlayerPrefs.SetString("user.inviteCode", inviteCode);
        PlayerPrefs.SetInt("user.isRewardInvite", isRewardInvite);
        PlayerPrefs.SetInt("user.inviteRewardTimes", inviteRewardTimes);
    }

}
