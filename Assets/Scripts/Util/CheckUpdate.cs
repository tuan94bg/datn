﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.IO;
//using UnityEngine.Experimental.Networking;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using Newtonsoft.Json;

public class CheckUpdate : MonoBehaviour
{
    public GameObject ready;
    public GameObject fader;
    public GameObject LoginButton;
    public Text nameFacebook;
    public ChooseAccRow accountOnLocal;
    public ChooseAccRow accountOnDB;
    public Toggle accountOnLocalToggle;
    public Toggle accountOnDBToggle;
    public GameObject panelChooseAccount;
    public GameObject panelLoading;

    private User userOnDB;

    private float timeFader = 1f;
    private int maxLevelOnServer = -1;
    public bool nextScene = false;
    public bool isLogging = false;

    private string idFb;
    private string name;

    Dictionary<string, string> headers = new Dictionary<string, string>();
    void Start()
    {
        maxLevelOnServer = GameData.Instance.MaxLevel;
        SoundController.Instance.PlayBackgroundLoginMusic();
        //StartCoroutine (Check ());
        //StartCoroutine(SynCheckAllLevel());
        //StartCoroutine(SyncGetMaxLevel());
        //Login();
        FB.Init(SetInit, OnHideUnity);
        //Ready();
        if (!string.IsNullOrEmpty(GameData.Instance.nameFace))
        {
            ShowName(true);
        }
        else
        {
            ShowName(false);
        }
    }
    public void ShowName(bool isShow)
    {
        //LoginButton.SetActive(!isShow);
        nameFacebook.text = GameData.Instance.nameFace;
        nameFacebook.gameObject.SetActive(isShow);
    }
    private void SetInit()
    {
        //  FBLogin();

        if (FB.IsLoggedIn)
        {
            Debug.Log("Logged in");
        }
    }

    private void OnHideUnity(bool isGameshow)
    {
        if (isGameshow)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void FBLogin()
    {
        //Login("3");
        SoundController.Instance.PlayButtonPress();
        if (!nextScene && !isLogging)
        {
            FB.LogInWithReadPermissions(new List<string>() { "public_profile" }, LoginFacebookCallback);
            isLogging = true;
        }
    }
    private void LoginFacebookCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            // AccessToken class will have session details
            var aToken = AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(aToken.UserId);
            idFb = aToken.UserId;
            
            
            FB.API("me?fields=first_name", HttpMethod.GET, UserCallBack);
            //Login("3");
            // Print current access token's granted permissions
            foreach (string perm in aToken.Permissions)
            {
                Debug.Log(perm);
            }
        }
        else
        {
            Debug.Log("User cancelled login");
        }
       
    }
    void UserCallBack(IGraphResult result)
    {
        IDictionary<string,object> dict = result.ResultDictionary;
        name = dict["first_name"].ToString();
        isLogging = false;
        //Login(idFb);
    }
    public void Login(string idFb)
    {
        Dictionary<string, string> sentData = new Dictionary<string, string>();
        sentData.Add(CommonTags.USER_IDFB, idFb);
        GameHub.Instance.userHub.Login(JsonConvert.SerializeObject(sentData), LoginCallBack);
        //GameHub.Instance.Test(JsonConvert.SerializeObject(sentData));
    }
    public void LoginCallBack(string data)
    {
        isLogging = false;
        Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        ResponseCode responsecode = (ResponseCode)Enum.Parse(typeof(ResponseCode), result[CommonTags.RESPONSE_CODE]);
        Debug.Log(responsecode);
        if (responsecode == ResponseCode.USER_NOT_EXIST)
        {
            User newUser = GameData.Instance.userProfile;
            newUser.idFB = idFb;
            GameHub.Instance.userHub.NewUser(newUser.ToString(), NewUserCallBack);
        }
        else if (responsecode == ResponseCode.USER_EXIST)
        {
            Debug.Log(result[CommonTags.USER_DB]);
            userOnDB = JsonConvert.DeserializeObject<User>(result[CommonTags.USER_DB]);
            if (GameData.Instance.userProfile.id < 0 || GameData.Instance.userProfile.idFB == userOnDB.idFB)
            {
                panelChooseAccount.SetActive(true);
                accountOnDB.SetInfo(userOnDB);
                accountOnLocal.SetInfo(GameData.Instance.userProfile);
            }
            else
            {
                GameData.Instance.userProfile = userOnDB;
                GameData.Instance.SaveDataUser();
                GameData.Instance.SetNameFacebook(name);
                ShowName(true);
            }
        }
    }

    public void ChooseUser()
    {
        if (accountOnLocalToggle.isOn)
        {
            GameHub.Instance.userHub.SetUser(GameData.Instance.userProfile.ToString(), SetUserCallBack);
        }
        else
        {
            GameHub.Instance.userHub.SetUser(userOnDB.ToString(), SetUserCallBack);
        }
        GameData.Instance.SetNameFacebook(name);
        ShowName(true);
    }
    public void SetUserCallBack(string data)
    {
        Debug.Log(data);
        GameData.Instance.SetNameFacebook(name);
        ShowName(true);
    }
    public void NewUserCallBack(string data)
    {
        Debug.Log(data);
        Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        ResponseCode responsecode = CommonFormat.ConvertToResponseCode(result[CommonTags.RESPONSE_CODE]);
        Debug.Log(responsecode);
        if (responsecode == ResponseCode.SUCCESS)
        {
            userOnDB = JsonConvert.DeserializeObject<User>(result[CommonTags.USER_DB]);
            GameData.Instance.userProfile = userOnDB;
            GameData.Instance.SaveDataUser();
            GameData.Instance.SetNameFacebook(name);
            ShowName(true);
        }
        else
        {
            // TODO : THONG BAO KHONG THE ASSOCIATION ACCOUNT
            Debug.Log("Khong the them USER");
        }
    }
    public void Ready()
    {
        ready.SetActive(true);
    }

    void Update()
    {
        //if (ready.activeInHierarchy && Input.GetMouseButtonDown(0))
        //{
        //    StartCoroutine(TouchOnScreen());
        //}
        if (nextScene)
        {
            {
                fader.SetActive(true);
            }
            if (fader.activeInHierarchy)
            {
                timeFader -= Time.deltaTime;
            }
            if (timeFader <= 0)
            {
                SceneManager.LoadScene("World");
                timeFader = 1000f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (NoticeDialog.Instance.isShowing)
            {
                NoticeDialog.Instance.Cancel();
            }
            else
            {
                NoticeDialog.Instance.ShowDialog("Do you want quit game?", Application.Quit, null, true);
            }
        }
    }
    
    public void TouchOnScreenBtn()
    {
        if (ready.activeInHierarchy)
        {
            StartCoroutine(TouchOnScreen());
        }
    }

    IEnumerator TouchOnScreen()
    {
        yield return new WaitForEndOfFrame();
        if (!isLogging)
        {
            nextScene = true;
        }
    }

    IEnumerator Check()
    {
        string result = string.Empty;
        WWW test = new WWW("http://127.0.0.1:8080/SYNC_GET_MAXLEVEL/1");
        yield return test;
        if (test.error == null)
        {
            result = test.text;
        }
        Debug.Log(result);
        if (!string.IsNullOrEmpty(result))
        {
            int maxLevel;
            if (int.TryParse(result, out maxLevel))
            {
                Debug.Log(maxLevel);
            }
            else
            {
                Debug.Log("Some Error");
            }
        }
    }

    private IEnumerator SyncGetMaxLevel()
    {
        string result = string.Empty;
        WWW test = new WWW("http://127.0.0.1:8080/SYNC_GET_MAXLEVEL/1");
        yield return test;
        if (test.error == null)
        {
            result = test.text;
            Debug.Log(result);
            if (!string.IsNullOrEmpty(result))
            {
                int maxLevel;
                if (int.TryParse(result, out maxLevel))
                {
                    maxLevelOnServer = maxLevel;
                    if (maxLevelOnServer > GameData.Instance.MaxLevel)
                    {
                        StartCoroutine(SynCheckAllLevel());
                    }
                }
                else
                {
                    Debug.Log("Can't Parse");
                }
            }
        }
        else
        {
            Debug.Log(test.error);
        }
    }
    private IEnumerator SynCheckAllLevel()
    {
        for (int i = 1; i < maxLevelOnServer; i++)
        {
            Debug.Log(i + " " + maxLevelOnServer);
            if (!GameData.IsExistOnLocal(i))
            {
                Debug.Log("khong co" + i);
                WWW collider = new WWW("http://127.0.0.1:8080/SYNC_GET_COLLIDER/" + i);
                yield return collider;
                Debug.Log(collider.text);
                //GameData.SaveFileTextOnDevice(collider.text, GameData.defaultStageName + i);
                WWW mapImg = new WWW("http://127.0.0.1:8080/SYNC_GET_MAP_IMG/" + i);
                yield return mapImg;
                GameData.SaveFileImageOnDevice(mapImg.texture, GameData.defaultStageName + i);

                //UnityWebRequest www = UnityWebRequest.Get("http://127.0.0.1:8080/SYNC_GET_COLLIDER/"+i);
                //yield return www.Send();
                //if (www.isError)
                //{
                //    Debug.Log(www.error);
                //}
                //else
                //{
                //    System.Text.StringBuilder headerBuilder = new System.Text.StringBuilder();
                //    if (www.GetResponseHeaders()!=null)
                //        foreach (KeyValuePair<string, string> entry in www.GetResponseHeaders())
                //            headerBuilder.AppendLine(entry.Key + "=" + entry.Value);

                //    Debug.Log(headerBuilder.ToString());
                //    Debug.Log(www.responseCode);
                //    Debug.Log(www.GetHashCode());
                //    // Show results as text
                //    Debug.Log(www.downloadHandler.text);

                // Or retrieve results as binary data
                //byte[] results = www.downloadHandler.data;
                //}

            }
        }
        yield break;
    }



    
}

public enum Method
{
    SYNC_GET_MAXLEVEL = 0,
    SYNC_GET_COLLIDER = 1,
    SYNC_GET_MAP_IMG = 2
}
