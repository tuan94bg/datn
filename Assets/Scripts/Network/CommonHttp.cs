﻿using UnityEngine;
using System.Collections;
using System.Net;

public class CommonHttp
{

    public static string host = "127.0.0.1";
    public static string port = "8080";
    public static string uriHttp = "http://";
    public static string serverUrl
    {
        get
        {
            return (uriHttp + host + ":" + port);
        }
    }
    public static string NewUri(METHOD method, string parameter)
    {
        string result = uriHttp+host+":"+port+"/" + method.ToString()+"/" + parameter;
        return result;
    }
    public static string NewUriPost(METHOD method)
    {
        string result = uriHttp + host + ":" + port + "/" + method.ToString();
        return result;
    }
    public static string NewUri(string method, string parameter)
    {
        string result = uriHttp + host + ":" + port + "/" + method + "/" + parameter;
        result = WWW.EscapeURL(result);
        return result;
    }
}