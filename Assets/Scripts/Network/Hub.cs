﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.IO;
using System.Text;
using System.Collections.Specialized;
using UnityEngine.Events;
using System;
using System.Threading;

public class Hub{
    private void RequestServerCall(METHOD method, string data, UnityAction<string> callBack = null)
    {
        ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
#if UNITY_EDITOR
        Debug.Log(CommonHttp.serverUrl);
#endif
        WebRequest req = WebRequest.Create(CommonHttp.serverUrl);
        HttpWebRequest request = (HttpWebRequest)req;
        request.ContentType = "application/x-www-form-urlencoded";
        request.Method = "POST";

        NameValueCollection nvc = new NameValueCollection();
        nvc.Add(((int)PostData.METHOD).ToString(), ((int)method).ToString());
        if (data != null)
        {
            nvc.Add(((int)PostData.DATA).ToString(), data);
        }
        StringBuilder postVars = new StringBuilder();
        foreach (string key in nvc)
            postVars.AppendFormat("{0}={1}&", key, nvc[key]);
        postVars.Length -= 1; // clip off the remaining &


        //This
        //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        //    streamWriter.Write(postVars.ToString());
        WebResponse resp = null;
        //Or this works
        try
        {
            var streamWriter = new StreamWriter(request.GetRequestStream());
            streamWriter.Write(postVars.ToString());
            streamWriter.Close();

            resp = req.GetResponse();
        }catch (Exception ex)
        {
            Debug.LogError(ex.Message);
        }
        if (resp == null)
        {
            GameHub.Instance.isConnected = false;
            if (callBack != null)
            {
                CallBack.Instance.AddCallBack(callBack, null);
            }
        }
        else
        {
            System.IO.StreamReader sr =
                  new System.IO.StreamReader(resp.GetResponseStream());
            string result = sr.ReadToEnd().Trim();
#if UNITY_EDITOR
            Debug.Log(result);
#endif
            if (callBack != null)
            {
                CallBack.Instance.AddCallBack(callBack,result);
            }
        }
    }

    public void CreateRequestServerThread(METHOD method, string data, UnityAction<string> callBack = null)
    {
        Thread oThread = new Thread(() => RequestServerCall(method, data, callBack));
        oThread.Start();
    }

    private static bool TrustCertificate(object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors)
    {
        // all Certificates are accepted
        return true;
    }
}
