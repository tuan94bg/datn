﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
//using UnityEngine.Experimental.Networking;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System;
using System.Collections.Generic;

public class GameHub
{
    private static GameHub instance;
    public static GameHub Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameHub();
            }
            return instance;
        }
    }

    public bool isConnected = true;
    public MapHub mapHub;
    public RewardHub rewardHub;
    public UserHub userHub;

    public GameHub()
    {
        mapHub = new MapHub();
        rewardHub = new RewardHub();
        userHub = new UserHub();
    }
    public IEnumerator Login(string data, UnityAction<string> CallBack)
    {
        string uri = CommonHttp.NewUri(METHOD.SYNC_LOGIN, data);
#if UNITY_EDITOR
        Debug.Log(uri);
#endif
        WWW request = new WWW(uri);
        yield return request;
        CallBack(request.text);
    }
    public IEnumerator SetUser(string data, UnityAction<string> CallBack)
    {
        string uri = CommonHttp.NewUri(METHOD.SYNC_SET_USER, data);
#if UNITY_EDITOR
        Debug.Log(uri);
#endif
        WWW request = new WWW(uri);
        yield return request;
        CallBack(request.text);
    }
    public IEnumerator NewUser(string data, UnityAction<string> CallBack)
    {
        string uri = CommonHttp.NewUri(METHOD.SYNC_NEW_USER, data);
#if UNITY_EDITOR
        Debug.Log(uri);
#endif
        Debug.Log(data);
        WWW request = new WWW(uri);
        yield return request;
        CallBack(request.text);
    }

    public void SetLevelOnDB()
    {
        Dictionary<string, string> dataDic = new Dictionary<string, string>();
        dataDic.Add(CommonTags.USER_IDFB, GameData.Instance.userProfile.idFB);
        dataDic.Add(CommonTags.LEVEL, GameData.Instance.userProfile.level.ToString());
        userHub.SetLevel(CommonFormat.GetResultFromDictionary(dataDic), null);
    }

    public void SetCoin()
    {
        Dictionary<string, string> dataDic = new Dictionary<string, string>();
        dataDic.Add(CommonTags.USER_IDFB, GameData.Instance.userProfile.idFB);
        dataDic.Add(CommonTags.COIN, GameData.Instance.userProfile.coin.ToString());
        userHub.SetCoin(CommonFormat.GetResultFromDictionary(dataDic), null);
    }

    public IEnumerator SetLevelOnDBAsyn()
    {
        Dictionary<string, string> dataDic = new Dictionary<string, string>();
        dataDic.Add(CommonTags.USER_IDFB, GameData.Instance.userProfile.idFB);
        dataDic.Add(CommonTags.LEVEL, GameData.Instance.userProfile.level.ToString());
        userHub.SetLevel(CommonFormat.GetResultFromDictionary(dataDic), null);
        yield return new WaitForEndOfFrame();
    }

    public IEnumerator SetCoinOnDB(string data, UnityAction<string> CallBack)
    {
        string uri = CommonHttp.NewUri(METHOD.SYNC_SET_COIN, data);
        WWW request = new WWW(uri);
        yield return request;
        CallBack(request.text);
    }
    public IEnumerator CallServer(METHOD method, string data, UnityAction<string> CallBack)
    {
        string uri = CommonHttp.NewUri(method, data);
        WWW request = new WWW(uri);
        yield return request;
        CallBack(request.text);
    }


    public void Test(string data)
    {
        string uri = CommonHttp.NewUriPost(METHOD.SYNC_LOGIN);
        RequestServer(METHOD.SYNC_LOGIN, data);
    }
    public IEnumerator Wait(WWW www)
    {
        while (!www.isDone)
        {
            yield return new WaitForFixedUpdate();
        }
        Debug.Log(www.text);
    }
    public void RequestServer(METHOD method, string data, UnityAction<string> callBack = null)
    {
        ServicePointManager.ServerCertificateValidationCallback = TrustCertificate;
        WebRequest req = WebRequest.Create(CommonHttp.serverUrl);
        HttpWebRequest request = (HttpWebRequest)req;
        request.ContentType = "application/x-www-form-urlencoded";
        request.Method = "POST";

        NameValueCollection nvc = new NameValueCollection();
        nvc.Add(((int)PostData.METHOD).ToString(), ((int)method).ToString());
        if (data != null)
        {
            nvc.Add(((int)PostData.DATA).ToString(), data);
        }
        StringBuilder postVars = new StringBuilder();
        foreach (string key in nvc)
            postVars.AppendFormat("{0}={1}&", key, nvc[key]);
        postVars.Length -= 1; // clip off the remaining &


        //This
        //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        //    streamWriter.Write(postVars.ToString());

        //Or this works
        var streamWriter = new StreamWriter(request.GetRequestStream());
        streamWriter.Write(postVars.ToString());
        streamWriter.Close();

        System.Net.WebResponse resp = req.GetResponse();
        if (resp == null)
        {
            Debug.Log("Empty");
            if (callBack != null)
            {
                callBack("Empty");
            }
        }
        else
        {
            System.IO.StreamReader sr =
                  new System.IO.StreamReader(resp.GetResponseStream());
            string result = sr.ReadToEnd().Trim();
            Debug.Log(result);
            if (callBack != null)
            {
                callBack(result);
            }
        }
    }
    private static bool TrustCertificate(object sender, X509Certificate x509Certificate, X509Chain x509Chain, SslPolicyErrors sslPolicyErrors)
    {
        // all Certificates are accepted
        return true;
    }
}
