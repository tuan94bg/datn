﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class RewardHub : Hub {
    public void GetBonusDay(UnityAction<string> action)
    {
        CreateRequestServerThread(METHOD.SYNC_GET_BONUS, null, action);
    }

    public void TryGetInviteReward(string data, UnityAction<string> action)
    {
        CreateRequestServerThread(METHOD.SYNC_INVITE_CODE_REWARD, data, action);
    }
}
