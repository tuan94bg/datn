﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class MapHub : Hub{

    public void Test()
    {

    }
    public void GetMaxLevel(UnityAction<string> action)
    {
        CreateRequestServerThread(METHOD.SYNC_GET_MAXLEVEL, null, action);
    }
    public void GetLevel(string data,UnityAction<string> action)
    {
        CreateRequestServerThread(METHOD.SYNC_GET_LEVEL, data, action);
    }

}
