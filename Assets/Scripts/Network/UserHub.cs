﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class UserHub : Hub {

    public void Login(string data, UnityAction<string> CallBack) 
    {
        CreateRequestServerThread(METHOD.SYNC_LOGIN, data, CallBack);
    }
    public void NewUser(string data, UnityAction<string> CallBack)
    {
        CreateRequestServerThread(METHOD.SYNC_NEW_USER, data, CallBack);
    }
    public void SetUser(string data, UnityAction<string> CallBack)
    {
        CreateRequestServerThread(METHOD.SYNC_SET_USER, data, CallBack);
    }
    public void SetLevel(string data, UnityAction<string> CallBack)
    {
        CreateRequestServerThread(METHOD.SYNC_SET_LEVEL, data, CallBack);
    }
    public void SetCoin(string data, UnityAction<string> CallBack)
    {
        CreateRequestServerThread(METHOD.SYNC_SET_COIN, data, CallBack);
    }
}
