﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using System;
using System.Threading;

public class MapUpdate : MonoBehaviour
{
    public GameObject ready;
    public GameObject updateQuestion;

    public GameObject panelLoading;
    public Image loadingIndicator;

    public Text host;

    private Vector3 clockwise = new Vector3(0, 0, 6);
    private int levelOnServer;
    void Start()
    {
        levelOnServer = GameData.Instance.MaxLevel;
        Debug.Log(CommonHttp.host);
        panelLoading.SetActive(true);
        CheckUpdate();
        //GetLevelImg(1);
        //Thread oThread = new Thread(CheckLevel);
        //oThread.Start();
    }
    void FixedUpdate()
    {
        loadingIndicator.rectTransform.Rotate(clockwise);
    }

    private void CheckUpdate()
    {
        //  StartCoroutine(CheckLevel());
        CheckLevel();
    }


    public void CheckLevel()
    {
        GameHub.Instance.mapHub.GetMaxLevel(CheckLevelCallBack);
    }
    public void GetLevel(int levelID)
    {
        Dictionary<string, string> postData = new Dictionary<string, string>();
        postData.Add(CommonTags.LEVEL, levelID.ToString());
        GameHub.Instance.mapHub.GetLevel(CommonFormat.GetResultFromDictionary(postData), GetLevelCallBack);
    }
    private void CheckLevelCallBack(string arg0)
    {
        panelLoading.SetActive(false);
        if (!string.IsNullOrEmpty(arg0))
        {
            Dictionary<string, string> dic = CommonFormat.JsonDataToDictionary(arg0);
            ResponseCode responseCode = CommonFormat.ConvertToResponseCode(dic[CommonTags.RESPONSE_CODE]);
            if (responseCode == ResponseCode.SUCCESS)
            {

                GameHub.Instance.rewardHub.GetBonusDay(GameData.Instance.GetBonusCallBack);
                levelOnServer = int.Parse(dic[CommonTags.LEVEL]);
#if UNITY_EDITOR
                Debug.Log(levelOnServer + "  " + GameData.Instance.MaxLevel);
#endif
                if (levelOnServer > GameData.Instance.MaxLevel)
                {
                    NoticeDialog.Instance.ShowDialog("New Map !\n Update now?", ConfirmDowloadMap, Ready, true);
                }
                else
                {
                    Ready();
                }
            }
            else
            {
                NoticeDialog.Instance.ShowDialog("Error:" + responseCode, Ready, null, false);
            }
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("No connections to server game!" + arg0, Ready, null, false);
        }
        // TODO : Thong bao khong co mang
    }
    public void ConfirmDowloadMap()
    {
        bool isDownloading = false;
        panelLoading.SetActive(true);
        for (int i = 1; i <= levelOnServer; i++)
        {
            if (!GameData.IsExistOnLocal(i))
            {
                isDownloading = true;
                GetLevel(i);
            }
            else
            { 
                if (i>GameData.Instance.MaxLevel)
                {
                    GameData.Instance.SaveMaxLevel(i);
                }
            }
        }
        if (isDownloading == false)
        {
            Ready();
        }
    }
    private void GetLevelCallBack(string arg0)
    {
        Debug.Log(arg0);
        Dictionary<string, string> dic = CommonFormat.JsonDataToDictionary(arg0);
        ResponseCode responseCode = CommonFormat.ConvertToResponseCode(dic[CommonTags.RESPONSE_CODE]);
        int levelJustDownload = int.Parse(dic[CommonTags.LEVEL]);
        if (responseCode == ResponseCode.SUCCESS)
        {
            string colliderData = dic[CommonTags.MAP_COL];
            byte[] dataCollider = Convert.FromBase64String(colliderData);
            GameData.SaveFileTextOnDevice(Encoding.ASCII.GetString(dataCollider), GameData.defaultStageName + levelJustDownload);

            string imgData = dic[CommonTags.MAP_IMG];
            byte[] dataImg = Convert.FromBase64String(imgData);
            Texture2D texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(dataImg);
            GameData.SaveFileImageOnDevice(texture, GameData.defaultStageName + levelJustDownload);


            if (levelJustDownload > GameData.Instance.MaxLevel)
            {
                GameData.Instance.SaveMaxLevel(levelJustDownload);
            }
            if (levelJustDownload == levelOnServer)
            {
                Ready();
            }
        }
        else
        {
            // TODO : Thong bao khong the cap nhat
            NoticeDialog.Instance.ShowDialog("Can not update!", Ready, null, false);
        }
    }
    public void Ready()
    {
        panelLoading.SetActive(false);
        ready.SetActive(true);
    }

    public void ChangeHost()
    {
        if (!string.IsNullOrEmpty(host.text))
        {
            CommonHttp.host = host.text;
        }
    }
}
