﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Events;

public class CallBack : MonoBehaviour {

    private static CallBack instance;
    public static CallBack Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<CallBack>();
            }
            return instance;
        }
    }

    public Queue<KeyValuePair<UnityAction<string>, string>> listCallBack = new Queue<KeyValuePair<UnityAction<string>, string>>();

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);
    }
    public void AddCallBack(UnityAction<string> callback, string resultData)
    {
        listCallBack.Enqueue(new KeyValuePair<UnityAction<string>, string>(callback, resultData));
    }
    void Update()
    {
        if (listCallBack.Count == 0) return;
        KeyValuePair<UnityAction<string>, string> callback = listCallBack.Dequeue();
        callback.Key.Invoke(callback.Value);
    }
}
