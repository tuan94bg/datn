﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChooseAccRow : MonoBehaviour {
    public Text nameTxt;
    public Text coinTxt;
    public Text levelTxt;
    public Text inviteCodeTxt;

    public void SetInfo(string name, int coin, int level, string inviteCode)
    {
        nameTxt.text = name;
        coinTxt.text = coin.ToString();
        levelTxt.text = level.ToString();
        inviteCodeTxt.text = inviteCode;
    }
    public void SetInfo(User user)
    {
        nameTxt.text = user.name;
        coinTxt.text = user.coin.ToString();
        levelTxt.text = user.level.ToString();
        inviteCodeTxt.text = user.inviteCode;
    }

}
