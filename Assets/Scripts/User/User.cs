﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;

public class User
{
    public int id = -1;
    public string idFB = string.Empty;
    public string name = string.Empty;
    public int coin = 0;
    public int level = 0;
    public string inviteCode = string.Empty;
    public int isRewardInvite = 0; // 0 = false, 1 = true
    public int inviteRewardTimes = 5; // Times of rewraed when invite other - Logic max 5
    public User()
    {
    }

    public User(string data)
    {
        data = data.Replace("\\\"", "\"");
        data = data.Replace("NULL", "\"\"");
        Console.WriteLine("DATA" + data);
        User user = JsonConvert.DeserializeObject<User>(data);
        this.id = user.id;
        this.idFB = user.idFB;
        //this.name = data[1];
        this.coin = user.coin;
        this.level = user.level;
        this.inviteCode = user.inviteCode;
        this.isRewardInvite = user.isRewardInvite;
        this.inviteRewardTimes = user.inviteRewardTimes;
    }

    public string ToJSon()
    {
        string result = string.Empty;
        result = JsonConvert.SerializeObject(this);
        return result;
    }
    public override string ToString()
    {
        string result = string.Empty;
        result = CommonFormat.AddContent(CommonTags.USER_DB, ToJSon());
        Dictionary<string, string> resultDic = new Dictionary<string, string>();
        resultDic.Add(CommonTags.USER_DB, ToJSon());
        result = JsonConvert.SerializeObject(resultDic);
        Debug.Log(result);
        return result;
    }
}
