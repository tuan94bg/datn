﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class VirtualJoystick : MonoBehaviour, IDragHandler,IPointerUpHandler, IPointerDownHandler {
	private Image bgimg;
	private Image joystickImg;
	private Vector3 inputVector;

	private void Start(){
		bgimg =GetComponent<Image>();
		joystickImg = transform.GetChild(0).GetComponent<Image>();
	}
	public virtual void OnDrag(PointerEventData ped){
		Vector2 pos;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgimg.rectTransform
		                                                            ,ped.position
		                                                            ,ped.pressEventCamera
		                                                            ,out pos))
			{
				pos.x = (pos.x / bgimg.rectTransform.sizeDelta.x);
				pos.y = (pos.y / bgimg.rectTransform.sizeDelta.y);

			inputVector = new Vector3(pos.x*2,0,pos.y*2);
			inputVector = (inputVector.magnitude>1f?inputVector.normalized:inputVector);
			//Debug.Log(pos);

			joystickImg.rectTransform.anchoredPosition = 
				new Vector3(inputVector.x*(bgimg.rectTransform.sizeDelta.x/2)
				            ,inputVector.z*(bgimg.rectTransform.sizeDelta.y/2));
			}
	}
	public virtual void OnPointerDown(PointerEventData ped){
		OnDrag(ped);
	}
	public virtual void OnPointerUp(PointerEventData ped){
		inputVector = Vector3.zero;
		joystickImg.rectTransform.anchoredPosition = Vector3.zero;
	}

	public float Horizantal(){
		if (inputVector.x!=0)
			return inputVector.x;
		else 
			return Input.GetAxis("Horizontal");
	}
	public float Vertical(){
		if (inputVector.z!=0)
			return inputVector.z;
		return Input.GetAxis("Vertical");
	}
}
