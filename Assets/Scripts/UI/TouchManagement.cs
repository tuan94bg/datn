﻿using UnityEngine;
using System.Collections;

public class TouchManagement : MonoBehaviour {

	public CharacterController player;
	public RectTransform moveLeftButton;
	public RectTransform moveRightButton;
	public RectTransform sitButton;
	public RectTransform jumpButton;

	void Update()
	{
		if (Input.touchCount>0)
		{
			Touch touch = Input.GetTouch(0);
			if (Vector2.Distance(touch.position,jumpButton.rect.center)<jumpButton.rect.height/2f)
				player.Jump();		
		}
	}
}
