﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class NoticeDialog : MonoBehaviour
{
    public GameObject panelNotice;
    public Text message;
    public GameObject cancelButton;
    public Canvas canvas;

    private UnityAction confirmAction;
    private UnityAction cancelAction;

    public bool isShowing
    {
        get
        {
            return panelNotice.activeInHierarchy;
        }
    }

    private static NoticeDialog instance;
    public static NoticeDialog Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<NoticeDialog>();
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void OnLevelWasLoaded()
    {
        //Canvas cnv = FindObjectOfType<Canvas>();
        //if (cnv != null)
        //{
        //    transform.parent = FindObjectOfType<Canvas>().transform;
        //}
        //panelNotice.SetActive(false);
    }

    public void ShowDialog(string Message, UnityAction actionConfirm, UnityAction actionCancel = null, bool isShowCancel = true)
    {
        panelNotice.SetActive(true);
        if (isShowCancel)
        {
            cancelButton.SetActive(true);
        }
        else
        {
            cancelButton.SetActive(false);
        }

        message.text = Message;
        confirmAction = actionConfirm;
        cancelAction = actionCancel;
    }

    public void Confirm()
    {
        SoundController.Instance.PlayButtonPress();
        if (confirmAction != null)
        {
            confirmAction.Invoke();
        }
        panelNotice.SetActive(false);
    }
    public void Cancel()
    {
        SoundController.Instance.PlayButtonPress();
        if (cancelAction != null)
        {
            cancelAction.Invoke();
        }
        panelNotice.SetActive(false);
    }
}
