﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StageResult : MonoBehaviour
{

    public Text TitleText;
    public Image fade;
    public Text totalScore;
    public Text coinBonus;
    public GameObject loseMessage;
    public Image batman;
    public GameObject toWorld;

    private Color color = new Color(0f, 0f, 0f, 0);
    private bool isSet = false;
    void Update()
    {
        if (isSet)
        {
            return;
        }
        batman.color = GameController.Instance.isWin ? Color.white : Color.red;
        color.a = Mathf.Clamp(color.a + Time.deltaTime * 2f, 0, 1);
        fade.color = color;
        if (color.a == 1)
        {
            TitleText.gameObject.SetActive(true);
            toWorld.SetActive(true);
            if (GameController.Instance.isWin)
            {
                totalScore.gameObject.SetActive(true);
                coinBonus.gameObject.SetActive(true);
                TitleText.text = "MISSION SUCCESS";
                StartCoroutine(ShowBonus(GameController.Instance.score));
            }
            else
            {
                TitleText.text = "GAME OVER";
                loseMessage.SetActive(true);
            }
            isSet = true;
        }
    }

    IEnumerator ShowBonus(int Score)
    {
        for (int i = 0; i <= Score; i += 5)
        {
            totalScore.text = "TOTAL SCORE : " + i;
            coinBonus.text = "COIN BONUS : " + i / 1000;
            yield return new WaitForEndOfFrame();
        }
        GameData.Instance.userProfile.coin += Score / 1000;
        totalScore.text = "TOTAL SCORE : " + Score;
    }

    public void ToWorld()
    {
        GameData.Instance.ResetForNewStage();
        SceneManager.LoadScene("World");
    }
}
