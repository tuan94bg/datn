﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{

    static SoundController instance;

    public static SoundController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<SoundController>();
            }
            return instance;
        }
    }

    public AudioSource music;
    public AudioSource musicWorld;
    public AudioSource musicLogin;
    public AudioSource[] musicStage;
    public AudioSource musicEndGame;
    public AudioSource buttonPress;

    private AudioSource currentMusic;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        currentMusic = music;
        this.PlayBackgroundMusic();
    }

    void OnDestroy()
    {
        instance = null;
    }

    public void PlayBackgroundMusic()
    {
        currentMusic.Stop();
        this.currentMusic.volume = GameData.Instance.setting.isMuteMusic == 0 ? 1f : 0;
        this.currentMusic.Play();
    }
    public void PlayBackgroundWorldMusic()
    {
        currentMusic.Stop();
        this.musicWorld.volume = GameData.Instance.setting.isMuteMusic == 0 ? 1f : 0;
        this.musicWorld.Play();
        this.currentMusic = musicWorld;
    }
    public void PlayBackgroundLoginMusic()
    {
        currentMusic.Stop();
        this.musicLogin.volume = GameData.Instance.setting.isMuteMusic == 0 ? 1f : 0;
        this.musicLogin.Play();
        currentMusic = musicLogin;
    }

    public void PlayBackgroundStageMusic(int stage)
    {
        if (stage >= musicStage.Length)
        {
            stage = stage % musicStage.Length+1;
        }
        currentMusic.Stop();
        this.musicStage[stage - 1].volume = GameData.Instance.setting.isMuteMusic == 0 ? 1f : 0;
        this.musicStage[stage - 1].Play();
        currentMusic = musicStage[stage - 1];
    }

    public void PlayerBackGroundEndGame()
    {
        currentMusic.Stop();
        this.musicEndGame.volume = GameData.Instance.setting.isMuteMusic == 0 ? 1f : 0;
        this.musicEndGame.Play();
        currentMusic = musicEndGame;
    }

    public void PlayButtonPress()
    {
        if (this.buttonPress.isPlaying == false)
        {
            this.buttonPress.volume = GameData.Instance.setting.isMuteSound == 0 ? 1f : 0;
            this.buttonPress.Play();
        }
    }

}
