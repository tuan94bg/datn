﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BuyPanel : MonoBehaviour
{

    public Text StageName;
    public Text nameDayBonus;
    public Text ammo;
    public Text ammoBonus;
    public Text hp;
    public Text hpBonus;

    public int stageNumber;

    void Start()
    {
        ShowBonusDay();
    }

    void ShowBonusDay()
    {
        nameDayBonus.text = GameData.Instance.bonusday.name;

        if (GameData.Instance.bonusday.bonusHp > 0)
        {
            hpBonus.text = "+" + GameData.Instance.bonusday.bonusHp.ToString();
            GameData.Instance.MaxHealth += GameData.Instance.bonusday.bonusHp;
        }
        else
        {
            hpBonus.text = string.Empty;
        }

        if (GameData.Instance.bonusday.bonusAmmo > 0)
        {
            ammoBonus.text = "+" + GameData.Instance.bonusday.bonusAmmo.ToString();
            GameData.Instance.startBullet += GameData.Instance.bonusday.bonusAmmo;
        }
        else
        {
            ammoBonus.text = string.Empty;
        }
    }

    void OnEnable()
    {
        ShowInfo();
    }

    public void ShowInfo()
    {
        StageName.text = "Stage " + GameInit.levelIndex;
    }

    public void BuyHP()
    {
        if (GameData.Instance.hpBuy < 5)
        {
            if (GameData.Instance.userProfile.coin <= 0)
            {
                NoticeDialog.Instance.ShowDialog("Not Enought Coin!\nWatch Video Now ?", WathVideoConfirm, null, true);
            }
            else
            {
                GameData.Instance.hpBuy++;
                GameData.Instance.MaxHealth++;
                GameData.Instance.userProfile.coin--;
                hp.text ="Health : " + GameData.Instance.MaxHealth.ToString();
                HeaderMenu.Instance.ShowInfo();
            }
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("Max times buy Health !", null, null, false);
        }

        GameData.Instance.SaveCoin();
    }

    private void WathVideoConfirm()
    {
        StartCoroutine(WatchVideo());
    }

    IEnumerator WatchVideo()
    {
        yield return new WaitForEndOfFrame();
        int coin = UnityEngine.Random.Range(1, 4);
        GameData.Instance.userProfile.coin += coin;
        NoticeDialog.Instance.ShowDialog(string.Format("Watch Video Success.\nYou have {0} coin bonus", coin), null, null, false);
        HeaderMenu.Instance.ShowInfo();
        GameData.Instance.SaveCoin();
    }

    public void BuyAmmo()
    {
        if (GameData.Instance.ammoBuy < 100)
        {
            if (GameData.Instance.userProfile.coin <= 0)
            {
                NoticeDialog.Instance.ShowDialog("Not Enought Coin!\nWatch Video Now ?", WathVideoConfirm, null, true);
            }
            else
            {
                GameData.Instance.ammoBuy += 5;
                GameData.Instance.startBullet += 5;
                GameData.Instance.userProfile.coin--;
                ammo.text ="Ammo :" + GameData.Instance.startBullet.ToString();
                HeaderMenu.Instance.ShowInfo();
            }
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("Max times buy ammo!", null, null, false);
        }
        GameData.Instance.SaveCoin();
    }

    public void ShowBonus()
    {

    }
    public void Cancel()
    {
        gameObject.SetActive(false);
    }
    public void Play()
    {
        SceneManager.LoadScene("Loading");
    }
}
