﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GiftcodePanel : MonoBehaviour
{
    public Text code;
    public void Claim()
    {
        TryGetInviteGift();
    }

    public void Cancel()
    {
        HeaderMenu.Instance.CloseGiftPanel();
    }

    public void TryGetInviteGift()
    {
        if (GameData.Instance.userProfile.id >= 0)
        {
            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add(CommonTags.USER_IDFB, GameData.Instance.userProfile.idFB);
            postData.Add(CommonTags.INVITE_CODE, code.text.ToUpper());
            postData.Add(CommonTags.COIN, GameData.Instance.userProfile.coin.ToString());
            GameHub.Instance.rewardHub.TryGetInviteReward(CommonFormat.GetResultFromDictionary(postData), GetInviteCodeCallBack);
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("Not login yet!\nLogin now?", MoveToLogin, Cancel, true);
        }
    }

    private void GetInviteCodeCallBack(string arg0)
    {
        if (string.IsNullOrEmpty(arg0))
        {
            NoticeDialog.Instance.ShowDialog("No connections!", null, null, false);
        }
        else
        {
            Dictionary<string, string> result = CommonFormat.JsonDataToDictionary(arg0);
            ResponseCode responseCode = CommonFormat.ConvertToResponseCode(result[CommonTags.RESPONSE_CODE]);

            switch (responseCode)
            {
                case ResponseCode.SUCCESS:
                    NoticeDialog.Instance.ShowDialog("Congratulate! You have receive 20 coin!", Cancel, null, false);
                    GameData.Instance.userProfile.coin += 20;
                    GameData.Instance.SaveCoin();
                    HeaderMenu.Instance.ShowInfo();
                    break;
                case ResponseCode.REWARDED:
                    NoticeDialog.Instance.ShowDialog("You may only receive 1 times", Cancel, null, false);
                    break;
                case ResponseCode.CODE_WRONG:
                    NoticeDialog.Instance.ShowDialog("Your code wrong or invalid!", null, null, false);
                    break;
                case ResponseCode.CODE_EXPIRES:
                    NoticeDialog.Instance.ShowDialog("Your code expired!", null, null, false);
                    break;
                case ResponseCode.FAILED:
                    NoticeDialog.Instance.ShowDialog("Unknow Error!", null, null, false);
                    break;
            }
        }
    }

    private void MoveToLogin()
    {
        HeaderMenu.Instance.BackOnClick();
    }
}
