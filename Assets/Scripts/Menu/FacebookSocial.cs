﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;

public class FacebookSocial : MonoBehaviour
{

    private const string SCREENSHOT_NAME = "Batman.png";

    static FacebookSocial instance;

    public static FacebookSocial Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<FacebookSocial>();
            }
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        //DontDestroyOnLoad(this);
        FB.Init();
    }
    void OnDestroy()
    {
        instance = null;
    }
    public void InviteFacebookFriend(FacebookDelegate<IAppRequestResult> callback = null)
    {
        if (FB.IsInitialized)
        {
            if (FB.IsLoggedIn)
            {
                //FB.Mobile.AppInvite(appUri, previewImgUri, InviteFriendCallback);

                FB.AppRequest(
                    "Come play this great game!",
                    null, new List<object>() { "app_users" }, null, null, null, null,
                    callback
                );
            }
            else
            {
                // "public_profile", "email", "user_friends", "publish_actions"
                var perms = new List<string>() { "public_profile", "email", "user_friends" };
                FB.LogInWithReadPermissions(perms, callback: delegate(ILoginResult result)
                {
                    if (FB.IsLoggedIn)
                        FB.AppRequest(
                            "Come play this great game!",
                            null, null, null, null, null, null,
                            callback
                        );
                });
            }
        }
    }

    public void ShareFacebook(string message, byte[] screenShotData, FacebookDelegate<IGraphResult> callback = null)
    {
        if (FB.IsInitialized)
        {
            //if (FB.IsLoggedIn)
            //{
            //    WWWForm wwwForm = new WWWForm();
            //    wwwForm.AddBinaryData("image", screenShotData, SCREENSHOT_NAME);
            //    wwwForm.AddField("name", message);
            //    FB.API("me/photos", HttpMethod.POST, callback, wwwForm);
            //}
            //else
            {
                //FB.LogOut();
                HeaderMenu.Instance.sharePanel.ShowMessage("Chua dang nhap");
                var perms = new List<string>() { "publish_actions" };
                FB.LogInWithPublishPermissions(perms, callback: delegate(ILoginResult result)
                {
                    HeaderMenu.Instance.sharePanel.ShowMessage(result.Error);
                    if (FB.IsLoggedIn)
                    {
                        WWWForm wwwForm = new WWWForm();
                        wwwForm.AddBinaryData("image", screenShotData, SCREENSHOT_NAME);
                        wwwForm.AddField("name", message);
                        FB.API("me/photos", HttpMethod.POST, callback, wwwForm);
                    }
                });
            }
        }
        //else
        //{
        //    FB.Init(onInitComplete: delegate()
        //    {
        //        if (FB.IsLoggedIn)
        //        {
        //            WWWForm wwwForm = new WWWForm();
        //            wwwForm.AddBinaryData("image", screenShotData, SCREENSHOT_NAME);
        //            wwwForm.AddField("name", message);
        //            FB.API("me/photos", HttpMethod.POST, callback, wwwForm);
        //        }
        //    });
        //}
    }
}
