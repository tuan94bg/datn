﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class HeaderMenu : MonoBehaviour {
    public SharePanel sharePanel;

    public RectTransform settingPanel;

    public GiftcodePanel panelGift;

    public Text userName;
    public Text userCoin;

    public Toggle soundMute;
    public Toggle musicMute;

    private static HeaderMenu instance;
    public static HeaderMenu Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<HeaderMenu>();
            }
            return instance;
        }
    }

    private bool isOpenSetting = false;


    public void Awake()
    {
        instance = this;
        ShowInfo();
        if (GameData.Instance.setting.isMuteMusic == 1)
        {
            musicMute.isOn = true;
        }
        if (GameData.Instance.setting.isMuteSound == 1)
        {
            soundMute.isOn = true;
        }
    }

    public void ShowInfo()
    {
        userCoin.text = GameData.Instance.userProfile.coin.ToString();
        userName.text = GameData.Instance.userProfile.name;
    }

    public void OnDestroy()
    {
        instance = null;
    }


    public void BackOnClick()
    {
        SoundController.Instance.PlayButtonPress();
        SceneManager.LoadScene("Login");
    }

    public void OpenOrCloseSetting()
    {
        SoundController.Instance.PlayButtonPress();
        if (isOpenSetting)
        {
            CloseSetting();
            isOpenSetting = false;
        }
        else
        {
            OpenSetting();
            isOpenSetting = true;
        }

    }
    public void OpenSetting()
    {
        LeanTween.cancelAll();
        LeanTween.moveX(settingPanel, 0, 0.5f).setIgnoreTimeScale(true).setEase(LeanTweenType.easeOutCirc);
    }

    public void CloseSetting()
    {
        LeanTween.cancelAll();
        LeanTween.moveX(settingPanel, settingPanel.rect.width, 1).setIgnoreTimeScale(true);
    }

    public void InviteFriend()
    {
        SoundController.Instance.PlayButtonPress();
        FacebookSocial.Instance.InviteFacebookFriend(null);
        // TODO : Callback
    }


    public void OpenPanelShare()
    {
        SoundController.Instance.PlayButtonPress();
        sharePanel.gameObject.SetActive(true);
    }


    public void CloseSharePanel()
    {
        SoundController.Instance.PlayButtonPress();
        sharePanel.gameObject.SetActive(false);
    }

    public void OpenGiftPanel()
    {
        SoundController.Instance.PlayButtonPress();
        if (GameData.Instance.userProfile.id<=0)
        {

           // return;
        }
        if (GameData.Instance.userProfile.isRewardInvite == 0)
        {
            panelGift.gameObject.SetActive(true);
        }
        else
        {
            return;
        }
    }

    public void CloseGiftPanel()
    {
        panelGift.gameObject.SetActive(false);
    }
    
    public void SoundSetting(bool isMute)
    {
        if (isMute)
        {
            GameData.Instance.setting.isMuteSound = 1;
        }
        else
        {
            GameData.Instance.setting.isMuteSound = 0;
        }
    }

    public void MusicSetting(bool isMute)
    {
        if (isMute)
        {
            GameData.Instance.setting.isMuteMusic = 1;
            GameData.Instance.SaveSetting();
            SoundController.Instance.PlayBackgroundMusic();
        }
        else
        {
            GameData.Instance.setting.isMuteMusic = 0;
            GameData.Instance.SaveSetting();
            SoundController.Instance.PlayBackgroundMusic();
        }
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (NoticeDialog.Instance.isShowing)
            {
                NoticeDialog.Instance.Cancel();
            }
            else if (sharePanel.gameObject.activeInHierarchy)
            {
                CloseSharePanel();
            }
            else if (panelGift.gameObject.activeInHierarchy)
            {
                CloseGiftPanel();
            }
            else if (isOpenSetting)
            {
                CloseSetting();
            }
            else
            {
                if (GameController.Instance != null)
                {
                    GameController.Instance.menuController.PauseOrResume();
                }
            }
        }
    }
}
