﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class SharePanel : MonoBehaviour
{
    public Text name;
    public Text level;
    public Text coin;
    public Text inviteCode;

    public Text status;
    public Toggle includeLink;

    public RectTransform optionShare;
    private byte[] screenShotData = null;

    private string linkGame = "DATN - Hoang Minh Tuan - 20122674 - DHBKHN";


    public void SetValueShare()
    {
        name.text = "Name\n" + (GameData.Instance.userProfile.name == string.Empty ? GameData.Instance.nameFace : GameData.Instance.userProfile.name);
        level.text = "Level\n" + GameData.Instance.userProfile.level.ToString();
        coin.text = "Coin\n" + GameData.Instance.userProfile.coin.ToString();
        inviteCode.text = GameData.Instance.userProfile.inviteCode;
    }
    public void ShareNow()
    {
        string msg = status.text;
        if (includeLink.isOn)
        {
            msg += " ___________ \n" + linkGame;
        }
        FacebookSocial.Instance.ShareFacebook(msg, screenShotData, CallBack);
        // TODO : Callback
    }

    private void CallBack(Facebook.Unity.IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error))
        {
            NoticeDialog.Instance.ShowDialog("Share facebook ERROR :" + result.Error, null, null, false);
        }
        else if (result.Cancelled)
        {
            NoticeDialog.Instance.ShowDialog("You have cancel share!", null, null, false);
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("Share facebook success", null, null, false);
        }
    }
    public void OnEnable()
    {
        LeanTween.cancelAll();
        optionShare.anchoredPosition = Vector3.zero;
        StartCoroutine(ShowOption());
    }

    IEnumerator ShowOption()
    {
        SetValueShare();
        yield return new WaitForSeconds(0.2f);
        yield return new WaitForEndOfFrame();
        CaptureScreenShot();
        LeanTween.moveY(optionShare, optionShare.rect.height, 0.5f);
    }

    private void CaptureScreenShot()
    {
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

        screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);

        screenTexture.Apply();

        screenShotData = screenTexture.EncodeToPNG();
        File.WriteAllBytes(Application.persistentDataPath + "/" + "1.png", screenShotData);
    }

    public void Cancel()
    {
        HeaderMenu.Instance.CloseSharePanel();
    }

    public void ShowMessage(string msg)
    {
        if (msg == null)
        { return; }
        inviteCode.text = inviteCode.text + "\n" + msg;
    }
}
