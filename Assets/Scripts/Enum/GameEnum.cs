﻿using UnityEngine;
using System.Collections;

public enum METHOD
{
    NOTHING = -1,
    SYNC_GET_MAXLEVEL = 0,
    SYNC_GET_LEVEL = 1,
   // SYNC_GET_MAP_IMG = 2,
    SYNC_GET_USER = 3,
    SYNC_SET_USER = 4,
    SYNC_NEW_USER = 5,
    SYNC_GET_BONUS = 6,
    SYNC_LOGIN = 7,
    SYNC_SET_LEVEL = 8,
    SYNC_SET_COIN = 9,
    SYNC_INVITE_CODE_REWARD = 10,
    SYNC_INVITE_FACE_REWARD = 11,
    SYNC_SHARE_REWARD = 12,
    SYNC_CHECK_REWARD_CODE_INVITE = 13
}
