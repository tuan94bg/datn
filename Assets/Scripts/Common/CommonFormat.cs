﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
public class CommonFormat
{
    public static string AddContent(string commontag, string content)
    {
        string result = string.Empty;
        result = string.Format("\"{0}\" : \"{1}\"", commontag, content);
        return result;
    }
    public static ResponseCode ConvertToResponseCode(string code)
    {
        try
        {
           ResponseCode responseCode = (ResponseCode)int.Parse(code);
           return responseCode;
        }
        catch (Exception ex)
        {
            return ResponseCode.FAILED;
        }
    }
    public static Dictionary<string, string> uriToData(string data)
    {
        Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        return result;
    }
    public static Dictionary<string, string> JsonDataToDictionary(string data)
    {
        Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
        return result;
    }
    public static string GetResultFromDictionary(Dictionary<string, string> result)
    {
        string resultString = string.Empty;
        resultString = JsonConvert.SerializeObject(result);
        return resultString;
    }
}
