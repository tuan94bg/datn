﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public enum ResponseCode
{
    SUCCESS = 0,
    FAILED = 1,
    USER_NOT_EXIST = 2,
    USER_EXIST = 3,
    USER_OLD = 4,
    USER_NEW = 5,
    REWARDED = 6,
    CODE_WRONG = 7,
    NOT_LOGIN = 8,
    CODE_EXPIRES = 9,
}

public enum LOGIN_METHOD
{
    GOOGLE_PLAY = 0,
    GAME_CENTER = 1,
    FACEBOOK = 2
}
public enum CONFIRM
{
    YES = 0,
    NO = 1,
    CANCEL = 2
}
public enum PostData
{
    METHOD = 0,
    DATA = 1
}