﻿using UnityEngine;
using System.Collections;

public class Enemy_Bomb : Enemy
{

    public float lastPosXPlayer = -1;
    IEnumerator move;

    protected override void Awake()
    {
        anim = GetComponent<Animator>();
        id = ENEMY_ID.BOMB;
        move = Move();
        gameObject.SetActive(false);
    }

    IEnumerator Move()
    {
        float deltax = 1;
        while (Mathf.Abs(deltax) > 0.1f)
        {
            deltax = transform.position.x - lastPosXPlayer;
            transform.Translate(Vector3.right * 0.03f * (deltax > 0 ? -1f : 1f));
            yield return new WaitForSeconds(0.03f);
        }
        Die();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.playerTrigger) == true && lastPosXPlayer == -1f)
        {
            Detected(other.transform.position.x);
        }
    }

    public override void Die()
    {
        //anim.SetTrigger("Detected");
        StopCoroutine(move);
        anim.SetBool("Die",true);
        lastPosXPlayer = -1f;
    }

    private void Detected(float x)
    {
        lastPosXPlayer = x;
        StartCoroutine(move);
    }
}
