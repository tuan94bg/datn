﻿using UnityEngine;
using System.Collections;
using DentedPixel;
public class Enemy_Flier : Enemy
{

    public JetOfFlier jet;
    public Enemy_Bullet[] enemyBullet;
    private Vector3 pointToShot;
    private Vector3 pointStart;
    private Vector3 pointPath1;
    private Vector3 pointPath2;
    private int indexBullet = 0;
    private bool isJustShot = false;
    private bool isStartMoveToShot = false;
    private float time;
    int id1, id2;
    private bool isPause = false;
    //private float countFrame;
    protected override void Awake()
    {

        base.Awake();
        id = ENEMY_ID.FLIER;
        health = 3;
    }

    public override void ClearForVisible()
    {
        base.ClearForVisible();
        pointStart = transform.position;
        isJustShot = false;
        isStartMoveToShot = false;
    }
    void OnEnable()
    {
        StartCoroutine(Recoil());
        pointStart = transform.position;
        //CaculatorPath();
        // LeanTween.move(this.gameObject, new Vector3[] { pointStart, pointPath1, pointPath2, pointToShot }, 2f);
        // LeanTween.move(this.gameObject, new Vector3[] { Vector3.zero, Vector3.right*4f, Vector3.one*4f, Vector3.up*4f }, 10f);
    }

    private void CaculatorPath()
    {
        if (player.transform.position.x > transform.position.x && !playerRight)
        {
            Flip();
        }
        else if (player.transform.position.x < transform.position.x && playerRight)
        {
            Flip();
        }
        pointToShot = new Vector3(player.transform.position.x + 1f * (playerRight ? -1f : 1f), player.transform.position.y + 0.38f);
        pointPath1 = new Vector3(pointStart.x / 2f + pointToShot.x / 2f, pointStart.y);
        pointPath2 = new Vector3(pointToShot.x, pointStart.y / 2f + pointToShot.y / 2f);
        time = (pointToShot - pointStart).magnitude;
        if (time < 1f)
        {
            time = 1f;
        }
        id1 = LeanTween.move(this.gameObject, new Vector3[] { pointStart, pointPath1, pointPath2, pointToShot }, time).id;
        //id1 = lt.uniqueId;
    }

    public override void Die()
    {
        LeanTween.pause(id1);
        LeanTween.pause(id2);
        jet.DisableAll();
        base.Die();
    }

    void Update()
    {
        if (stop && !isPause)
        {
            LeanTween.pause(id1);
            LeanTween.pause(id2);
            isPause = true;
        }
        if (!stop && isPause)
        {
            LeanTween.resume(id1);
            LeanTween.resume(id2);
            isPause = false;
        }
        if (!stop && !isPause)
        {
            if ((transform.position - pointToShot).magnitude < 0.05f && !isJustShot)
            {
                isJustShot = true;
                isStartMoveToShot = false;
                Shoot();
            }
            else if ((transform.position - pointStart).magnitude < 0.01f && !isStartMoveToShot)
            {
                isJustShot = false;
                isStartMoveToShot = true;
                MoveToPointShot();
            }
        }
    }

    public void MoveBackStartPoint()
    {
        id2 = LeanTween.move(this.gameObject, new Vector3[] { pointToShot, pointPath2, pointPath1, pointStart }, time).id;
    }
    //IEnumerator MoveToShot()
    //{

    //        yield return new WaitForSeconds(0.5f);
    //        CaculatorPath();
    //}
    public void MoveToPointShot()
    {
        CaculatorPath();
    }
    //public override void Injured(int damage)
    //{
    //    base.Injured(damage);
    //}

    public void Shoot()
    {
        anim.SetTrigger("Shoot");
    }

    public void ShootBullet()
    {
        GameObject aBullet = enemyBullet[indexBullet].gameObject;

        aBullet.transform.position = transform.position + new Vector3(-0.2f * (playerRight ? -1f : 1f), 0.156f, 0f);
        enemyBullet[indexBullet].direction = new Vector3(playerRight ? 3f : -3f, -1f);
        aBullet.SetActive(true);
        aBullet.transform.parent = null;
        indexBullet++;
        if (indexBullet > 2)
        {
            indexBullet = 0;
        }
    }


    IEnumerator Recoil()
    {
        while (true)
        {
            if (!stop)
            {
                transform.Translate(Vector3.up * 0.01f);
            }
            yield return new WaitForSeconds(0.075f);
            if (!stop)
            {
                transform.Translate(Vector3.down * 0.01f);
            }
            yield return new WaitForSeconds(0.075f);
        }
    }

}
