﻿using UnityEngine;
using System.Collections;

public class Enemy_FireGunner : Enemy
{
    public GameObject fire;
    private bool canFlip = true;
    private bool canFlipRight = true;
    private bool canFlipLeft = true;
    protected override void Awake()
    {

        base.Awake();
        health = 3;
        id = ENEMY_ID.FIREGUNNER;
    }

    public override void ClearForVisible()
    {
        CheckCanFlip();
        base.ClearForVisible();
        if (!canFlipRight && playerRight)
        {
            base.Flip();
            canFlip = false;
        }
        if (!canFlipLeft && !playerRight)
        {
            base.Flip();
            canFlip = false;
        }
        health = 3;
    }

    void Update()
    {
        if (isActive)
        {
            if (!stop)
            {
                if (!canFlip)
                    return;
                if (transform.position.x > player.transform.position.x && playerRight)
                {
                    Flip();
                }
                else if (transform.position.x < player.transform.position.x && !playerRight)
                {
                    Flip();
                }
            }
            else EndFire();
        }
    }

    public void Fire()
    {
        fire.gameObject.SetActive(true);
    }

    public void EndFire()
    {
        fire.gameObject.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        CheckCollider(other);
    }

    public override void Die()
    {
        EndFire();
        base.Die();
    }
    public void Flip()
    {
        if (!canFlipRight && !playerRight)
        {
            return;
        }
        if (!canFlipLeft && playerRight)
        {
            return;
        }
        base.Flip();
        
        //  base.Flip();
    }
    public void CheckCanFlip()
    {
        RaycastHit2D[] hitRay;
        hitRay = Physics2D.RaycastAll(transform.position, transform.right, 0.3f);//, 18);//, 9);
        foreach (RaycastHit2D hit in hitRay)
        {
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag(Tags.MAP))
                {
                    canFlipRight = false;
                    canFlipLeft = true;
                    canFlip = false;

                    return;
                }
            }
        }

        hitRay = Physics2D.RaycastAll(transform.position, transform.right * -1f, 0.3f);//, 18);//, 9);
        foreach (RaycastHit2D hit in hitRay)
        {
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag(Tags.MAP))
                {
                    canFlipRight = true;
                    canFlipLeft = false;
                    return;
                }
            }
        }
        canFlipRight = true;
        canFlipLeft = true;
    }
}
