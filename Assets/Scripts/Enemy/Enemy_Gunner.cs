﻿using UnityEngine;
using System.Collections;

public class Enemy_Gunner : Enemy
{
    public Enemy_Bullet fire;

    protected override void Awake()
    {
        base.Awake();
        health = 3;
        id = ENEMY_ID.GUNNER;
    }

    void Update()
    {
        if (transform.position.x > player.transform.position.x && playerRight)
        {
            Flip();
        }
        else if (transform.position.x < player.transform.position.x && !playerRight)
        {
            Flip();
        }
    }

    public override void ClearForVisible()
    {
        base.ClearForVisible();
        health = 3;
    }

    public void Fire()
    {
        if (!stop)
        {
            fire.transform.parent = transform.parent;
            fire.transform.position = transform.position + new Vector3(0.13f * (playerRight ? 1 : -1), 0.226f, 0f);
            fire.gameObject.SetActive(true);
            fire.ChangeDirection(Vector3.right * (playerRight ? 1 : -1) * 2f);
        }
    }


    public void EndFire()
    {

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        CheckCollider(other);
    }
    public override void Die()
    {
        EndFire();
        base.Die();
    }

}
