﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Action
{
    public ActionType playerAction = ActionType.IDLE;
    public Vector3 playerPosition;
    public float timeStart;
    public Action()
    {
        GameController.Instance.playerController.CaculatorAction += CaculatorAction;
    }

    public void CaculatorAction(Vector2 pos, ActionType type)
    {
        playerPosition = pos;
        playerAction = type;
        timeStart = Time.realtimeSinceStartup;
    }
}

public enum ActionType
{
    IDLE = 0,
    MOVE_TO_OTHER = 1,
    MOVE_AWAY = 2,
    ATTACK = 3,
    JUMP_TO_OTHER = 4,
    JUMP_AWAY = 5,
    JUMP_IDLE = 6,
    SIT = 7,
    MOVE = 8
}

public class RatePointAction
{
    public Dictionary<ActionType, int> rateAction = new Dictionary<ActionType, int>();

    public void AddPointAction(ActionType type, int value)
    {
        rateAction.Add(type, value);
    }

    public int GetPoint(ActionType nextType)
    {
        return rateAction[nextType];
    }

    public int GetAllPoint()
    {
        int result = 0;
        foreach (KeyValuePair<ActionType, int> element in rateAction)
        {
            result += element.Value;
        }
        return result;
    }

    public ActionType GetTypeMax()
    {
        int maxValue = 0;
        ActionType result = ActionType.IDLE;
        foreach (KeyValuePair<ActionType, int> element in rateAction)
        {
            if (element.Value > maxValue)
            {
                maxValue = element.Value;
                result = element.Key;
            }
        }
        return result;
    }
}
