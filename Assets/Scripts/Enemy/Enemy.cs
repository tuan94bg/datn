﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{

    public int health = 1;
    public bool playerRight = false;
    [HideInInspector]
    public ENEMY_ID id;
    protected GameObject player;
    protected float speed = 0.75f;
    protected Rigidbody2D rig;
    protected SpriteRenderer sprite;
    protected Collider2D colTrigger;
    protected bool stop = false;
    protected bool isActive = true;
    protected float timeStop = 1f;
    protected Animator anim;
    protected virtual void Awake()
    {
        colTrigger = GetComponent<BoxCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
        rig = GetComponent<Rigidbody2D>();
        player = GameController.Instance.playerController.gameObject;
        anim = GetComponent<Animator>();
        transform.parent = GameController.Instance.poolContainer;
        this.name = GameController.Instance.poolController.EnemyID.ToString();
    }

    public virtual void ClearForVisible()
    {
        transform.localScale = Vector3.one;
        playerRight = false;
        if (transform.position.x < player.transform.position.x)
        {
            Flip();
        }
        isActive = true;
        if (rig != null)
        {
            rig.gravityScale = 0.7f;
            rig.isKinematic = false;
        }
        stop = false;
        anim.speed = 1;
        timeStop = 1f;
        sprite.color = new Color(255, 255, 255, 255);
        colTrigger.enabled = true;
        
    }

    public void Flip()
    {
        playerRight = !playerRight;
        Vector3 theScale = transform.localScale;
        theScale.x = playerRight ? -1 : 1;
        transform.localScale = theScale;
    }

    public void CheckCollider(Collider2D other)
    {

        if (Tags.bullet == other.tag)
        {
            Bullet otherBullet = other.gameObject.GetComponent<Bullet>();
            Injured(otherBullet.damage);
            if (otherBullet.id == BulletID.Rocket) otherBullet.OnBecameInvisible();
        }
    }
    protected void CheckHealth()
    {
        if (health <= 0)
        {
            GetComponent<Animator>().SetBool("Die", true);
            Die();
        }
        else
        {

            if (timeStop == 1f) StartCoroutine(StopAndFlicker());
            else timeStop = 1f;
        }

    }

    public void Injured(int damage)
    {
        health -= damage;
        CheckHealth();
    }


    public virtual void Die()
    {
        GameController.Instance.AddScoreKillEnemy(id);
        GameObject item = Poolling.Instance.GetItem();
        item.transform.position = transform.position + Vector3.up * 0.226f;
        item.SetActive(true);
        isActive = false;
        colTrigger.enabled = false;
        if (rig != null) rig.isKinematic = true;
        //		foreach (Collider2D col in GetComponents<Collider2D>())
        //		{
        //			col.enabled = false;
        //		}
    }
    protected void Hide()
    {
        isActive = false;
        //Poolling.Instance.ReturnEnemyPool(this);
        gameObject.SetActive(false);
    }

    protected IEnumerator StopAndFlicker()
    {
        anim.speed = 0;
        if (rig) rig.velocity = Vector2.zero;
        stop = true;
        //Sprite currentSprite = sprite.sprite
        while (timeStop > 0 && isActive)
        {
            sprite.color = new Color(255, 255, 255, 0);
            yield return new WaitForSeconds(0.1f);
            sprite.color = new Color(255, 255, 255, 255);
            yield return new WaitForSeconds(0.1f);
            timeStop -= 0.1f;
        }
        timeStop = 1f;
        anim.speed = 1;
        stop = false;
        sprite.enabled = true;
    }

    protected virtual void OnBecameInvisible()
    {
        Poolling.Instance.SaveEnemyBackPool(this);
    }
}
