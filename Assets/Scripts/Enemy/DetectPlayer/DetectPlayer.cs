﻿using UnityEngine;
using System.Collections;

public class DetectPlayer : MonoBehaviour {

    public Enemy thisInstanceEnemy;

    void OnTriggerEnter2D(Collider2D other)
    {
        thisInstanceEnemy.CheckCollider(other);
        if (other.CompareTag(Tags.playerTrigger) == true)
        {
             thisInstanceEnemy.Die();
        }
    }
}
