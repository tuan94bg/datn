﻿using UnityEngine;
using System.Collections;

public class JetOfFlier : MonoBehaviour 
{
    public GameObject Jet1;
    public GameObject Jet2;
    public GameObject Jet3;

    public void EnableJet1()
    {
        Jet1.SetActive(true);
        Jet2.SetActive(false);
        Jet3.SetActive(false);
    }
    public void EnableJet2()
    {
        Jet1.SetActive(false);
        Jet2.SetActive(true);
        Jet3.SetActive(false);
    }
    public void EnableJet3()
    {
        Jet1.SetActive(false);
        Jet2.SetActive(false);
        Jet3.SetActive(true);
    }
    public void DisableAll()
    {
        Jet1.SetActive(false);
        Jet2.SetActive(false);
        Jet3.SetActive(false);
    }

    public void Flicker()
    { 
    }

}
