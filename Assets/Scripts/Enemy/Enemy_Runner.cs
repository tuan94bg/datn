﻿using UnityEngine;
using System.Collections;

public class Enemy_Runner : Enemy 
{
	Vector3 moveDirection= Vector3.right;


	protected override void Awake ()
	{
		base.Awake ();
		id = ENEMY_ID.RUNNER;
		health = 1;
	}
		
	void Update()
	{
		if(!stop&&isActive)
		{
			Run();
            if (Mathf.Abs(rig.velocity.x) < moveDirection.x * speed/2f) 
            {
                Flip();
            }
		}
	}
	void Run(){
		
		rig.velocity = new Vector2(moveDirection.x*speed*(playerRight?1f:-1f),rig.velocity.y);
	}

	protected void OnTriggerEnter2D(Collider2D other)
	{
		CheckCollider(other);
	}

	public override void Die()
	{
		base.Die();
		rig.velocity = Vector2.zero;
	}
}
