﻿using UnityEngine;
using System.Collections;

public class Enemy_Flier_Bullet : MonoBehaviour
{
    public bool targetLeft = true;
    private Vector3 moveVector = new Vector3(3f, -1f);

    void OnEnable()
    {
        transform.parent = null;
        if (targetLeft)
        {
            moveVector.x = -3f;
        }
        else
        {
            moveVector.x = 3f;
        }
    }
    void Update()
    {
        transform.Translate(moveVector * Time.deltaTime / 2f);
    }

    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
