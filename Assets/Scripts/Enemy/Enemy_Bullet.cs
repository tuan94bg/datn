﻿using UnityEngine;
using System.Collections;

public class Enemy_Bullet : MonoBehaviour
{
    public Vector3 direction;
    public bool move = true;
    void Update()
    {
        if (gameObject.activeSelf && move)
            transform.Translate(direction * Time.deltaTime / 2);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Tags.MAP) == true)
        {
            gameObject.SetActive(false);
        }
    }

    public void ChangeDirection(Vector3 directionvt)
    {
        Vector3 scale = transform.localScale;
        if (directionvt.x < 0) scale.x = 1;
        else scale.x = -1f;
        transform.localScale = scale;
        direction = directionvt;
    }

    void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
