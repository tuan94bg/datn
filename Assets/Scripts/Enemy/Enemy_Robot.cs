﻿using UnityEngine;
using System.Collections;

public class Enemy_Robot : Enemy_Runner {

	// Code them fastrun

	protected override void Awake()
	{
		base.Awake();
		health =4;
		id = ENEMY_ID.ROBOT;
	}

	public override void ClearForVisible ()
	{
		base.ClearForVisible ();
		health = 4;
	}
		
//	void Update()
//	{
//		if(!stop&&isActive)
//		{
//			Run();
//			CheckHealth();
//		}
//	}
//	void Run(){
//
//		rig.velocity = new Vector2(moveDirection.x*speed*(playerRight?1f:-1f),rig.velocity.y);
//	}
    void OnTriggerEnter2D(Collider2D other)
    {
        CheckCollider(other);
        if (other.CompareTag(Tags.playerTrigger))
        {
            speed = 1.5f;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(Tags.playerTrigger))
        {
            speed = 0.75f;
        }
    }
}
