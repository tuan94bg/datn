﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChooseStage : MonoBehaviour
{
    //public Image backGround;
    public Image[] allNode;
    public Animator[] allAnim;
    public Sprite jokerIcon;
    public Sprite batmanIcon;

    public RectTransform buyAmmoPanel;
    public BuyPanel panelBuy;

    private int currentIndex = 0;
    private int currentWorld = 0;

    void Start()
    {
        currentIndex = GameData.Instance.userProfile.level;
        currentWorld = GameData.Instance.indexWorld;
        SoundController.Instance.PlayBackgroundWorldMusic();
        ShowProgressMission();
    }

    public void ShowProgressMission()
    {
        for (int i = 0; i < allNode.Length; i++)
        {
            if (i < currentIndex)
            {
                allNode[i].sprite = batmanIcon;
                allNode[i].color = new Color32(255, 255, 255, 255);
            }
            else if (i == currentIndex)
            {
                allAnim[i].enabled = true;
                allNode[i].color = new Color32(255, 255, 255, 255);
            }
        }
    }

    public void SelectedStage(int idStage)
    {
        if (GameData.IsStageExistOnResource(idStage + 1))
        {
            GameInit.isMapOnBuildPack = true;
        }
        else if (GameData.IsStageExistOnDevice(idStage + 1))
        {
            GameInit.isMapOnBuildPack = false;
        }
        else
        {
            NoticeDialog.Instance.ShowDialog("This stage comming soon!", null, null, false);
            return;
        }

        GameInit.levelIndex = idStage + 1;
        //if (GameInit.levelIndex<=2)
        panelBuy.gameObject.SetActive(true);
        panelBuy.ShowInfo();
    }

}
