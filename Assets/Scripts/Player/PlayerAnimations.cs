﻿using UnityEngine;
using System.Collections;

public class PlayerAnimations : MonoBehaviour
{
    private AnimatorStateInfo currentState;
    private Animator anim;
    private HashID hashID;
    private SpriteRenderer playerSprite;
    private PlayerState playerState;

    private int fixCount;
    void Start()
    {
        playerState = GetComponent<CharacterController>().playerState;
        hashID = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashID>();
        anim = GetComponentInChildren<Animator>();
        playerSprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void TakeDamageColor()
    {
        StartCoroutine(TakeDamage());
    }
    IEnumerator TakeDamage()
    {
        playerSprite.color = Color.red;
        yield return new WaitForSeconds(1f);
        playerSprite.color = Color.white;
    }

    public void ChangeColorToRedBatMan()
    {
        playerSprite.color = Color.red;
    }

    void Update()
    {
        anim.SetFloat(hashID.vSpeedFloat, playerState.yVelocity);
        currentState = anim.GetCurrentAnimatorStateInfo(0);

        if (currentState.IsTag("Jumping"))
        {
            playerState.currentAnimation = AnimationState.Jumping;
            SetAnimFlying(true);
        }
        else
        {
            SetAnimFlying(false);
            if (currentState.IsTag("Bussy"))
            {
                playerState.currentAnimation = AnimationState.Bussy;
            }
            else if (currentState.IsTag("Sitting"))
            {
                playerState.currentAnimation = AnimationState.Sitting;
            }
            else
            {
                playerState.currentAnimation = AnimationState.Nothing;
            }
        }
    }

    public void SetImmortal()
    {
        anim.SetTrigger("Immortal");
    }
    public void AttackAnimation()
    {
        anim.SetTrigger("Attack");
    }
    public void ChangeAnimationAttack(int weaponID)
    {
        anim.SetInteger("Weapon", weaponID);
    }

    public void JumpOnGround()
    {
        anim.SetTrigger(hashID.jumpTrigger);
    }
    public void SetGroundBool(bool isGround)
    {
        anim.SetBool(hashID.groundBool, isGround);
    }

    public void Sit()
    {
        anim.SetBool("Sit", true);
    }
    public void EndSit()
    {
        anim.SetBool(hashID.sitBool, false);
        anim.SetBool(hashID.sittingBool, false);
    }

    public void Die()
    {
        anim.SetBool(hashID.isDieBool, true);
        anim.SetTrigger(hashID.isDieTrigger);
    }

    public void CallJumpTrigger()
    {
        anim.SetTrigger(hashID.jumpTrigger);
    }
    public void InitOnJumppingState(bool isIniting)
    {
        anim.SetBool(hashID.jumppingBool, isIniting);
    }

    public void JumpOnWall(bool isJumpOnWall)
    {
        anim.SetBool(hashID.jumpOnWallBool, isJumpOnWall);
    }

    public void SetAnimFlying(bool isFlying)
    {
        anim.SetBool("Flying", isFlying);
    }
    public void SetAnimSitting(bool isSitting)
    {
        anim.SetBool("Sitting", isSitting);
    }
    public void SetFloatSpeed(float speed)
    {
        anim.SetFloat(hashID.speedFloat, Mathf.Abs(speed));
    }
}
