﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using CnControls;
public class PlayerController : CharacterController {

    void Start()
    {
        gameObject.tag = Tags.player;
        playerState.isPlayer = true;
        playerState.bulletNumber = 200;
        playerState.Maxhealth = GameData.Instance.MaxHealth;
        playerState.health = playerState.Maxhealth;
        menuControl.ShowHealthMax(playerState.health);
    }

    
    public void Update()
    {
        base.UpdateAnimation();
        if (playerState.canControl)
        {
#if UNITY_STANDALONE
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Jump();
                }
                if (Input.GetKeyDown(KeyCode.L))
                {
                    NextWeapon(playerState.currentWeapon);
                }
                if (Input.GetKeyDown(KeyCode.K))
                {
                    Attack();
                }

                if (playerState.sitting && !playerState.bussy)
                {
                    SitAction();
                }
                else
                {
                    EndSitAction();
                }
                if (Input.GetKeyUp(KeyCode.S))
                {
                    EndSitAction();
                }
                if (Input.GetKey(KeyCode.D)) SetMoveVelocityValue(1f);
                if (Input.GetKey(KeyCode.A)) SetMoveVelocityValue(-1f);

                if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    playerPhysics.StopMoveX();
                    SetMoveVelocityValue(0);
                }
                if (playerState.sitting == true && !playerState.bussy && playerState.currentState != PLAYER_STATE.SITTING)
                {
                    SitAction();
                }
                else if (playerState.sitting == false && playerState.currentState == PLAYER_STATE.SITTING)
                {
                    EndSitAction();
                }
            }
#endif
            float horizontalValue = CnInputManager.GetAxis("Horizontal");
            float vertical = CnInputManager.GetAxis("Vertical");
            //Flip
            if (((horizontalValue > 0.3f && !playerState.facingRight) || (horizontalValue < -0.3f && playerState.facingRight)) && !playerState.jumpOnWall)
            {
                playerPhysics.Flip();
            }

            if (Mathf.Abs(horizontalValue)<0.6f)
            {
                horizontalValue = 0;
            }
            else
            {
                vertical = 0;
            }
            // Sit
            if (vertical < -0.6)
            {
                Sit();
            }
            else
            {
                EndSit();
            }
            //endSit
           
            SetMoveVelocityValue(horizontalValue);

            if (playerState.sitting && !playerState.bussy)
            {
                SitAction();
            }
            else
            {
                EndSitAction();
            }

            if (playerState.jumpOnWall)
            {
                if (playerState.grounded) playerState.jumpOnWall = false;
                //rigbd.AddForce(new Vector2(move * Time.deltaTime * 30, 0));
            }
            else
            {
                if (playerState.currentAnimation == AnimationState.Bussy || playerState.bussy)
                {
                    playerPhysics.StopMoveX();
                }
                else
                {
                    playerPhysics.Move();
                    if (Mathf.Abs(playerState.moveValue) == 1f)
                    {
                        EndSitAction();
                    }
                }
            }
            playerAnimations.SetFloatSpeed(playerState.moveValue);
            //if (((playerState.moveValue > 0.2f && !playerState.facingRight) || (playerState.moveValue < -0.2f && playerState.facingRight)) && !playerState.jumpOnWall)
            //{
            //    playerPhysics.Flip();
            //}
        }
    }

    public void Attack()
    {
        base.Attack();
        SendActionToAI(ActionType.ATTACK);
    }

    public override void Die()
    {
        // TODO : -1 mang
        GameController.Instance.GameOver();
    }
}
