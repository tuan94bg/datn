﻿using System;

public enum WEAPON_STATE
{
    PUNCH = 0,
    BOMERANG = 1,
    GUN = 2,
    SHURIKEN = 3
}
