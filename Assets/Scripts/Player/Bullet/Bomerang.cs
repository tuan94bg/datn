﻿using UnityEngine;
using System.Collections;

public class Bomerang : Bullet {
	public bool isAdd = false;
	private float speedX = 2.5f;
	private float speedY = -0.33f;
	private bool back=false;
	private float deltay=0.25f;
	private Rigidbody2D rig;
	//private GameObject player;
	protected void Awake () {
        base.Awake();
		id = BulletID.Bomerang;
		rig = GetComponent<Rigidbody2D>();
		//h = GameObject.FindGameObjectWithTag(Tags.player);
		gameObject.SetActive(false);
	}

	public void ChangeBmrState()
	{
		back = true;
	}

	public override void ResetState ()
	{
		speedX = 2.5f;
		speedY = -0.27f;
		rig.velocity = Vector2.zero;
		base.ResetState ();
		speedX *=facingRightPlayer;
	//	if (facingRightPlayer==-1)Flip();
		if (transform.position.y <hitman.transform.position.y+0.2f)
			deltay = 0.18f;
		back = false;

	}

	// Update is called once per frame 
	void FixedUpdate () {
		if (gameObject.activeInHierarchy)
		{
			speedX -= facingRightPlayer*0.1f;
			speedY += 0.03f;
			rig.velocity = new Vector2(speedX,speedY);
			if (back){
				float yDistance =  hitman.transform.position.y - transform.position.y+deltay;
				float xDistance =  hitman.transform.position.x - transform.position.x;
				if (Mathf.Abs(yDistance)>0f)
					rig.velocity = new Vector2(rig.velocity.x,yDistance*2*Mathf.Abs(rig.velocity.x/xDistance));
			}
		}
		if (rig.velocity.x*facingRightPlayer<0) back = true;
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		//Debug.Log("CL");
		if (col.tag == Tags.playerTrigger&&back) 
		{
			rig.velocity = Vector2.zero;
			OnBecameInvisible();
//			Poolling.Instance.ReturnBomerangPool(gameObject);
		}
	}

	public override void OnBecameInvisible ()
	{
		if (back) base.OnBecameInvisible();
	}
}
