﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
	public BulletID id;
	bool isActive = false;
	public int facingRightPlayer=1;
	public int damage=1;
    public CharacterController hitman;
    public SpriteRenderer sprite;

    protected virtual void Awake()
    {
        name = GameController.Instance.poolController.BulletIdName.ToString();
    }

    private void Init()
    {
        if (hitman.tag == Tags.player)
        {
            gameObject.tag = Tags.bullet;
            sprite.color = Color.white;
        }
        else
        {
            gameObject.tag = Tags.enemy_bullet;
            sprite.color = Color.red;
        }
    }
	public void Flip()
	{
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	public void Hide()
	{
//		Poolling.Instance.HideBullet(this);
	}
	public virtual void ResetState()
	{
        Init();
		transform.localScale = Vector3.one;
		if (facingRightPlayer==-1)Flip();
	}
	public virtual void OnBecameInvisible()
	{
		Poolling.Instance.SaveBulletBackPool(this);
	}
}

public enum BulletID
{
	Rocket = 1,
	Bomerang = 2,
	Bomb = 3
}
