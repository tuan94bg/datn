﻿using UnityEngine;
using System.Collections;

public class Bomb : Bullet
{
    public Vector3 moveDirection = new Vector3(3f, 0f);
    public bool parent = false;
    public float defautY;
    float speed = 1.2f;
    float timer;
    protected override void Awake()
    {
        base.Awake();
        id = BulletID.Bomb;
        gameObject.SetActive(false);
    }
    public override void ResetState()
    {
        timer = Time.time + 0.05f;
        parent = true;
        base.ResetState();
        moveDirection = new Vector3(3f, 0f);
        defautY = transform.position.y;
    }
    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            if (transform.position.y != defautY)
                moveDirection -= new Vector3(0, 0.6f * (transform.position.y - defautY) * facingRightPlayer);
            transform.Translate(moveDirection * Time.deltaTime * facingRightPlayer * speed);
            if (parent == true && timer < Time.time)
            {
                parent = false;
                StartCoroutine(DelayMove());
            }
        }
    }


    void Clone(Vector3 moveVector)
    {
        Bomb bombclone = (Bomb)Poolling.Instance.GetPlayerBulletFromPool(WEAPON_STATE.SHURIKEN);
        bombclone.parent = false;
        bombclone.moveDirection = moveVector;
        bombclone.facingRightPlayer = facingRightPlayer;
        bombclone.defautY = this.transform.position.y;
        bombclone.transform.position = transform.position;
        bombclone.gameObject.SetActive(true);
        Debug.Log("da ra roi");
    }

    IEnumerator DelayMove()
    {
        speed = 0.05f;
        yield return new WaitForSeconds(0.1f);
        speed = 1.2f;
        Clone(new Vector3(moveDirection.x, 1f));
        Clone(new Vector3(moveDirection.x, -1f));
    }

    //	public override void OnBecameInvisible ()
    //	{
    //		Poolling.Instance.HideBullet(this);
    //	}
}
