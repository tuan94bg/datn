﻿using UnityEngine;
using System.Collections;

public class Rocket : Bullet {

	//public int facingRightPlayer=1;
	protected override void Awake()
	{
        base.Awake();
		id = BulletID.Rocket;
		damage = 2;
		gameObject.SetActive(false);
	}
	void Update()
	{
		transform.Translate(Vector3.right*Time.deltaTime*facingRightPlayer*2);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag(Tags.MAP))
		{
			gameObject.SetActive(false);
		}
	}
}
