﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class CharacterController : MonoBehaviour
{

    public delegate void CaculaterAIAction(Vector2 pos, ActionType action);

    public event CaculaterAIAction CaculatorAction;

    public PlayerPhysics playerPhysics;
    public PlayerAnimations playerAnimations;
    public MenuControl menuControl;

    public PlayerState playerState;
    //public WEAPON_STATE weaponState = WEAPON_STATE.PUNCH;

    public Transform point_Normal;
    public Transform point_Jump;
    public Transform point_Sit;

    private bool checkFlyAndFall = false;

    public bool grounded;

    //private int payBullet = 0;

    void Awake()
    {
        //playerSprite = GetComponentInChildren<SpriteRenderer>();
        playerState = new PlayerState();
        playerState.controller = this;
        playerState.transformShowBullet = point_Normal;
        //CheckHealth();
    }

    protected void UpdateAnimation()
    {
        grounded = playerState.grounded;
        if (checkFlyAndFall && grounded && !playerState.canControl)
        {
            checkFlyAndFall = false;
            playerState.canControl = true;
        }
        playerAnimations.SetGroundBool(playerState.grounded);

        if (playerState.currentAnimation == AnimationState.Sitting)
        {
            playerAnimations.SetAnimSitting(true);
            playerPhysics.ChangeCollider(true);
        }
        else
        {
            playerAnimations.SetAnimSitting(false);
            playerPhysics.ChangeCollider(false);
        }
    }

    public void SetMoveVelocityValue(float value)
    {
        if (playerState.canControl)
        {
            playerState.moveValue = value;
            return;
        }
        playerState.moveValue = 0;
        SendActionToAI(ActionType.MOVE);
    }

    public void Jump()
    {
        if (playerState.canControl)
        {
            if (playerState.grounded && !playerState.jumpping)
            {
                playerAnimations.CallJumpTrigger();
                StartCoroutine(PopAndJumpOnGround(0.1f));
                ChangePlayerState(PLAYER_STATE.JUMP);
            }
            else if (playerState.canJumpOnWall
                && playerState.currentAnimation == AnimationState.Jumping
                && !playerState.immortal)
            {
                StartCoroutine(PopAndJumpOnWall());
            }
        }
    }

    private void ChangePlayerState(PLAYER_STATE state)
    {
        playerState.currentState = state;
        switch (playerState.currentState)
        {
            case PLAYER_STATE.JUMP:
                playerState.transformShowBullet = point_Jump;
                break;
            case PLAYER_STATE.NORMAL:
                playerState.transformShowBullet = point_Normal;
                break;
            case PLAYER_STATE.SITTING:
                playerState.transformShowBullet = point_Sit;
                break;
        }
    }

    public void Attack()
    {
        playerAnimations.AttackAnimation();
        int weaponStateInt = (int)playerState.currentWeapon;
        if (weaponStateInt > playerState.bulletNumber)
        {
            NextWeapon(WEAPON_STATE.SHURIKEN);
        }
        if (playerState.currentWeapon == WEAPON_STATE.PUNCH)
        {
            playerPhysics.Punch();
        }
        else
        {
            playerPhysics.AttackWithoutPunch(playerState.currentWeapon);
        }

        //switch (weaponState)
        //{
        //    case WEAPON_STATE.PUNCH:
        //        playerPhysics.Punch();
        //        break;
        //    case WEAPON_STATE.GUN:
        //        if (playerState.bulletNumber < 2)
        //        {
        //            NextWeapon(WEAPON_STATE.SHURIKEN);
        //            Attack();
        //        }
        //        else
        //        {
        //            playerPhysics.AttackWithoutPunch(weaponState);
        //        }
        //        break;
        //    case WEAPON_STATE.BOMERANG:
        //        if (playerState.bulletNumber < 1)
        //        {
        //            NextWeapon(WEAPON_STATE.SHURIKEN);
        //            Attack();
        //        }
        //        else
        //        {
        //            playerPhysics.AttackWithoutPunch(weaponState);
        //        }
        //        break;
        //    case WEAPON_STATE.SHURIKEN:
        //        if (playerState.bulletNumber < 3)
        //        {
        //            NextWeapon(WEAPON_STATE.SHURIKEN);
        //            Attack();
        //        }
        //        else
        //        {
        //            playerPhysics.AttackWithoutPunch(weaponState);
        //        }
        //        //	Skill(weaponState);
        //        break;
        //}
        if (playerState.isPlayer)
        {
            menuControl.ShowIconWeapon(playerState.bulletNumber, playerState.currentWeapon);
            SendActionToAI(ActionType.ATTACK);
        }
    }

    public void OnChangeWeapon()
    {
        NextWeapon(playerState.currentWeapon);
    }

    public void NextWeapon(WEAPON_STATE currentState)
    {
        int wstateint = (int)currentState;
        if (playerState.bulletNumber > wstateint)
        {
            playerState.currentWeapon = (WEAPON_STATE)(wstateint + 1 > 3 ? 0 : wstateint + 1);
        }
        else
        {
            playerState.currentWeapon = WEAPON_STATE.PUNCH;
        }
        //playerState.payBullet = wstateint;
        //switch (wState)
        //{
        //    case WEAPON_STATE.PUNCH:
        //        if (playerState.bulletNumber > 0)
        //        {
        //            weaponState = WEAPON_STATE.BOMERANG;
        //            playerState.payBullet = 1;
        //        }
        //        break;
        //    case WEAPON_STATE.BOMERANG:
        //        if (playerState.bulletNumber > 1)
        //        {
        //            weaponState = WEAPON_STATE.GUN;
        //            playerState.payBullet = 2;
        //        }
        //        break;
        //    case WEAPON_STATE.GUN:
        //        if (playerState.bulletNumber > 2)
        //        {
        //            weaponState = WEAPON_STATE.SHURIKEN;
        //            payBullet = 3;
        //        }
        //        break;
        //    case WEAPON_STATE.SHURIKEN:
        //        weaponState = WEAPON_STATE.PUNCH;
        //        payBullet = 0;
        //        break;
        //}
        if (playerState.isPlayer)
        {
            menuControl.ShowIconWeapon(playerState.bulletNumber, playerState.currentWeapon);
        }
        playerAnimations.ChangeAnimationAttack((int)playerState.currentWeapon);
    }

    public void Sit()
    {
        playerState.sitting = true;
    }

    public void EndSit()
    {
        playerState.sitting = false;
    }

    protected void SitAction()
    {
        playerAnimations.Sit();
        ChangePlayerState(PLAYER_STATE.SITTING);
        SendActionToAI(ActionType.SIT);
    }

    protected void EndSitAction()
    {
        playerAnimations.EndSit();
        ChangePlayerState(PLAYER_STATE.NORMAL);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (Tags.MAP == other.tag && playerState.currentAnimation == AnimationState.Jumping)
        {
            playerState.canJumpOnWall = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (Tags.MAP == other.tag)
        {
            playerState.canJumpOnWall = false;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //		Debug.Log(other.gameObject.layer);
        if (Tags.MAP == other.tag)
        {
            playerState.canJumpOnWall = true;
        }
        if (playerState.isPlayer)
        {
            if ((other.CompareTag(Tags.enemy) || Tags.enemy_bullet == other.tag) && !playerState.immortal)
            {
                playerState.health -= 1;
                StartCoroutine(TakeDamage());
            }

            if (("EndMap".CompareTo(other.tag) == 0))
            {
                GameController.Instance.LevelComplete();
            }
        }
        else
        {
            if (other.CompareTag(Tags.bullet)  && !playerState.immortal)
            {
                playerState.health -= 1;
                StartCoroutine(TakeDamage());
            }
        }
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(0);
    }

    public void TakeItem(ItemId id)
    {
        switch (id)
        {
            case ItemId.AMMO:
                playerState.bulletNumber += 10;
                if (playerState.isPlayer)
                {
                    menuControl.ShowIconWeapon(playerState.bulletNumber, playerState.currentWeapon);
                }
                break;
            case ItemId.HEALTH:
                playerState.health = playerState.health + 2 < 6 ? playerState.health + 2 : 6;
                if (playerState.isPlayer)
                {
                    menuControl.ShowHealth(playerState.health);
                }
                break;
            case ItemId.SCORE:
                GameController.Instance.score += 300;
                break;
        }
    }

    public void TakeDamagePunch()
    {
        playerState.health -= 1;
        StartCoroutine(TakeDamage());
    }

    IEnumerator TakeDamage()
    {
        if (playerState.isDead)
        {
            yield break;
        }
        checkFlyAndFall = false;
        playerState.immortal = true;
        if (playerState.isPlayer)
        {
            menuControl.ShowHealth(playerState.health);
        }
        else
        {
            EndSitAction();
            SetMoveVelocityValue(0);
        }
        playerState.canControl = false;
        if (playerState.health > 0)
        {
            playerPhysics.TakeDamage();
            playerAnimations.TakeDamageColor();
            playerAnimations.SetImmortal();
            yield return new WaitForSeconds(0.5f);
            checkFlyAndFall = true;
            yield return new WaitForSeconds(1.5f);
            playerState.immortal = false;
        }
        else
        {
            StartCoroutine(WaitDie());
            playerPhysics.Die();
            playerAnimations.Die();
            playerState.canControl = false;
        }
    }

    IEnumerator PopAndJumpOnGround(float time)
    {

        playerState.jumpping = true;
        playerState.bussy = true;

        playerAnimations.InitOnJumppingState(true);
        //playerAnimations.CallJumpTrigger();
        yield return new WaitForSeconds(time);
        playerPhysics.JumpOnGround();
        yield return new WaitForSeconds(time / 2);
        playerAnimations.InitOnJumppingState(false);
        playerState.jumpping = false;
        playerState.bussy = false;
    }

    IEnumerator PopAndJumpOnWall()
    {
        playerState.canJumpOnWall = false;
        playerState.jumpOnWall = true;

        playerAnimations.JumpOnWall(true);
        playerPhysics.OnWall();

        yield return new WaitForSeconds(0.25f);

        playerAnimations.JumpOnWall(false);
        playerPhysics.JumpOnWall();

    }

    public void ShowBug(string message)
    {
#if UNITY_EDITOR
        Debug.Log("abc");
#endif
    }

    IEnumerator WaitDie()
    {
        yield return new WaitForSeconds(2);
        Die();
        //SceneManager.LoadScene("World");
    }

    public virtual void Die() 
    {
        playerState.isDead = true;
    }

    public void SendActionToAI(ActionType type)
    {
        if (CaculatorAction != null)
        {
            CaculatorAction(transform.position, type);
        }
    }
}
