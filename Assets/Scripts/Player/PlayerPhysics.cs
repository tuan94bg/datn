﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerPhysics : MonoBehaviour
{

    #region PUBLIC_MEMBER

    public Transform groundCheck;
    public LayerMask whatisGround;
    public GameObject colliderNormal;
    public GameObject colliderSitting;
    //public Transform point_Normal;
    //public Transform point_Jump;
    //public Transform point_Sit;

    //public MenuControl menuControl;

    //public PLAYER_STATE playerState;
    //public WEAPON_STATE weaponState = WEAPON_STATE.PUNCH;
    public GameObject rocket;
    public GameObject bomerang;
    public GameObject bomb;
    //	public GameObject currentBullet;
    //	private Bullet currentBulletScripts;
    #endregion


    public PlayerState playerState;
    //public PlayerAnimations playerAnimation;

    private int payBullet;
    //private float move;
    #region PRIVATE_MEMBER

    private float groundCheckRadius = 0.07f;

    //private Transform currentPoint;

    int fixCount = 0;

    //private static PlayerController instance;
    //public static PlayerController Instance
    //{
    //    get 
    //    { 
    //        return instance;
    //    }
    //}
    //Animator anim;
    Rigidbody2D rigbd;
    //HashID hashID;
    //AnimatorStateInfo currentState;
    //SpriteRenderer playerSprite;

    #endregion
    #region UNITY_MONOBEHAVIUOR_METHOD

    void Start()
    {
        rigbd = GetComponent<Rigidbody2D>();
        playerState = GetComponent<CharacterController>().playerState;
    }


    void Update()
    {
        playerState.grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatisGround);
        playerState.yVelocity = rigbd.velocity.y;
    }
    //    void Update()
    //    {
    //        //currentState = anim.GetCurrentAnimatorStateInfo(0);
    //        playerState.grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatisGround);
    //        rigbd.velocity = new Vector2(move * playerState.speed, rigbd.velocity.y);
    //        //anim.SetBool(hashID.groundBool, playerState.grounded);
    //        //anim.SetFloat(hashID.vSpeedFloat, rigbd.velocity.y);
    //        //move = Input.GetAxis("Horizontal");



    //        //if (currentState.IsTag("Jumping"))
    //        //{
    //        //    anim.SetBool("Flying", true);
    //        //}
    //        //else
    //        //{
    //        //    anim.SetBool("Flying", false);
    //        //}
    //        ///// phai sua
    //        //if (currentState.IsTag("Sitting"))
    //        //{
    //        //    anim.SetBool("Sitting", true);
    //        //    ChangeCollider(true);
    //        //}
    //        //else
    //        //{
    //        //    anim.SetBool("Sitting", false);
    //        //    ChangeCollider(false);
    //        //}
    ////        if (playerState.canControl)
    ////        {
    ////#if UNITY_EDITOR
    ////            {
    ////                if (Input.GetKeyDown(KeyCode.Space))
    ////                {
    ////                    Jump();
    ////                }
    ////                if (Input.GetKeyDown(KeyCode.L))
    ////                {
    ////                    ChangeWeaponState(weaponState);
    ////                }
    ////                if (Input.GetKeyDown(KeyCode.K))
    ////                {
    ////                    Attack();
    ////                }

    ////                if (playerState.sitting && !playerState.bussy)
    ////                {
    ////                    SitAction();
    ////                }
    ////                else
    ////                {
    ////                    EndSitAction();
    ////                }
    ////                if (Input.GetKeyUp(KeyCode.S))
    ////                {
    ////                    EndSitAction();
    ////                }
    ////                if (Input.GetKey(KeyCode.D)) move = 1f;
    ////                if (Input.GetKey(KeyCode.A)) move = -1f;
    ////                rigbd.velocity = new Vector2(move * playerState.speed, rigbd.velocity.y);
    ////                if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
    ////                {
    ////                    rigbd.velocity.Scale(new Vector2(0f, 1f));
    ////                    move = 0;
    ////                }
    ////                if (playerState.sitting == true && !playerState.bussy && playerState.currentState != PLAYER_STATE.SITTING)
    ////                {
    ////                    SitAction();
    ////                }
    ////                else if (playerState.sitting == false && playerState.currentState == PLAYER_STATE.SITTING)
    ////                {
    ////                    EndSitAction();
    ////                }
    ////            }
    ////#endif
    ////            if (playerState.sitting && !playerState.bussy)
    ////            {
    ////                SitAction();
    ////            }
    ////            else
    ////            {
    ////                EndSitAction();
    ////            }

    ////            if (playerState.jumpOnWall)
    ////            {
    ////                if (playerState.grounded) playerState.jumpOnWall = false;
    ////                rigbd.AddForce(new Vector2(move * Time.deltaTime * 30, 0));
    ////            }
    ////            else
    ////            {
    ////                if (currentState.IsTag("Bussy") || playerState.bussy)
    ////                {
    ////                    rigbd.velocity = new Vector2(0f, rigbd.velocity.y);
    ////                }
    ////                else
    ////                {
    ////                    rigbd.velocity = new Vector2(move * playerState.speed, rigbd.velocity.y);
    ////                    if (Mathf.Abs(move) == 1f)
    ////                        EndSitAction();
    ////                }
    ////            }
    ////            anim.SetFloat(hashID.speedFloat, Mathf.Abs(move));
    ////            if (((move > 0.2f && !playerState.facingRight) || (move < -0.2f && playerState.facingRight)) && !playerState.jumpOnWall) Flip();
    ////        }
    //}


    #region Movement
    public void Move()
    {
        rigbd.velocity = new Vector2(playerState.moveValue * playerState.speed, rigbd.velocity.y);
    }
    public void StopMoveX()
    {
        rigbd.velocity = new Vector2(0f, rigbd.velocity.y);
    }

    public void JumpOnGround()
    {
        rigbd.AddForce(new Vector2(0f, 115f));
    }

    public void OnWall()
    {
        rigbd.velocity = new Vector2(0f, 0f);
        rigbd.gravityScale = 0;
    }
    public void JumpOnWall()
    {
        rigbd.AddForce(new Vector2(30f * (playerState.facingRight ? -1 : 1), 95f));
        Flip();
        rigbd.gravityScale = 0.5f;
    }

    //public void Jump()
    //{
    //    if (playerState.canControl)
    //    {
    //        if (playerState.grounded && !playerState.jumpping)
    //        {
    //            anim.SetTrigger(hashID.jumpTrigger);
    //            StartCoroutine(WaitStartJump(0.1f));
    //            ChangePlayerState(PLAYER_STATE.JUMP);
    //        }
    //        else if (playerState.canJumpOnWall && currentState.IsTag("Jumping") && !playerState.immortal)
    //        {
    //            playerState.jumpOnWall = true;
    //            anim.SetBool(hashID.jumpOnWallBool, true);
    //            StartCoroutine(OnWall());
    //        }
    //    }
    //}


    //public void Sit()
    //{
    //    playerState.sitting = true;
    //}

    //public void EndSit()
    //{
    //    playerState.sitting = false;
    //}
    //private void SitAction()
    //{
    //    anim.SetBool("Sit", true);
    //    ChangePlayerState(PLAYER_STATE.SITTING);
    //}

    //private void EndSitAction()
    //{
    //    anim.SetBool(hashID.sitBool, false);
    //    anim.SetBool(hashID.sittingBool, false);
    //    ChangePlayerState(PLAYER_STATE.NORMAL);
    //}

    #endregion

    //void OnTriggerStay2D(Collider2D other)
    //{
    //    if (Tags.MAP == other.tag && playerState.currentAnimation == AnimationState.Jumping)
    //    {
    //        playerState.canJumpOnWall = true;
    //    }
    //}
    //void OnTriggerExit2D(Collider2D other)
    //{
    //    if (Tags.MAP == other.tag)
    //    {
    //        playerState.canJumpOnWall = false;
    //    }
    //}
    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    //		Debug.Log(other.gameObject.layer);
    //    if (Tags.MAP == other.tag)
    //    {
    //        playerState.canJumpOnWall = true;
    //    }
    //    if ((other.CompareTag(Tags.enemy) || Tags.enemy_bullet == other.tag) && !playerState.immortal)
    //    {
    //        playerState.health -= 1;
    //        //			if (playerState.grounded)
    //        //			{
    //        //				StartCoroutine(TakeDamage(false));
    //        ////				rigbd.velocity = new Vector2(0f,rigbd.velocity.y);
    //        ////				if (!playerState.jumpping)
    //        ////				{
    //        ////					float force = playerState.facingRight?-30f:30f;
    //        ////					rigbd.AddRelativeForce(new Vector2(force,80));
    //        ////				}
    //        //			}
    //        //			else
    //        StartCoroutine(TakeDamage(false));
    //    }

    //    if (("EndMap".CompareTo(other.tag) == 0))
    //    {
    //        ResetScene();
    //    }
    //}

    //public void ResetScene()
    //{
    //    SceneManager.LoadScene(0);
    //}
    //public void TakeItem(ItemId id)
    //{
    //    switch (id)
    //    {
    //        case ItemId.AMMO:
    //            playerState.bulletNumber += 10;
    //            menuControl.ShowIconWeapon(playerState.bulletNumber, weaponState);
    //            break;
    //        case ItemId.HEALTH:
    //            playerState.health = playerState.health + 2 < 6 ? playerState.health + 2 : 6;
    //            menuControl.ShowHealth(playerState.health);
    //            break;
    //        case ItemId.SCORE:
    //            break;
    //    }
    //}

    #endregion

    #region MY_METHOD

    public void Flip()
    {
        // playerState.facingRight = !playerState.facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
        playerState.facingRight = theScale.x > 0 ? true : false;
    }


    public void EndJump()
    {

    }

    public void ChangeCollider(bool activeSit)
    {
        colliderSitting.SetActive(activeSit);
        colliderNormal.SetActive(!activeSit);
    }







    //private void ChangePlayerState(PLAYER_STATE state)
    //{
    //    playerState.currentState = state;
    //    switch (playerState.currentState)
    //    {
    //        case PLAYER_STATE.JUMP:
    //            currentPoint = point_Jump;
    //            break;
    //        case PLAYER_STATE.NORMAL:
    //            currentPoint = point_Normal;
    //            break;
    //        case PLAYER_STATE.SITTING:
    //            currentPoint = point_Sit;
    //            break;
    //    }
    //}

    //public void ChangeWeapon()
    //{
    //    ChangeWeaponState(weaponState);
    //}
    //public void ChangeWeaponState(WEAPON_STATE wState)
    //{
    //    switch (wState)
    //    {
    //        case WEAPON_STATE.PUNCH:
    //            if (playerState.bulletNumber > 0)
    //            {
    //                weaponState = WEAPON_STATE.SKILL1;
    //                payBullet = 1;
    //            }
    //            break;
    //        case WEAPON_STATE.SKILL1:
    //            if (playerState.bulletNumber > 1)
    //            {
    //                weaponState = WEAPON_STATE.GUN;
    //                payBullet = 2;
    //            }
    //            break;
    //        case WEAPON_STATE.GUN:
    //            if (playerState.bulletNumber > 2)
    //            {
    //                weaponState = WEAPON_STATE.SKILL2;
    //                payBullet = 3;
    //            }
    //            break;
    //        case WEAPON_STATE.SKILL2:
    //            weaponState = WEAPON_STATE.PUNCH;
    //            payBullet = 0;
    //            break;
    //    }
    //    menuControl.ShowIconWeapon(playerState.bulletNumber, weaponState);
    //    anim.SetInteger("Weapon", (int)weaponState);
    //}


    public void Punch()
    {
        StartCoroutine(AttackPunch());
    }
    public void AttackWithoutPunch(WEAPON_STATE weaponstate)
    {
        //weaponState = weaponstate;
        StartCoroutine(AttackWithoutPunch(playerState.currentState));
    }
    IEnumerator AttackWithoutPunch(PLAYER_STATE state)
    {
        playerState.bulletNumber -= (int)playerState.currentWeapon;
        //Debug.Log("playerState.bulletNumber : " + playerState.bulletNumber);
        //Debug.Log("payBullet : " + payBullet);
        Bullet currentBullet = Poolling.Instance.GetPlayerBulletFromPool(playerState.currentWeapon);
        currentBullet.facingRightPlayer = playerState.facingRight ? 1 : -1;
        //		Debug.Log(currentBullet.transform.position);
        yield return new WaitForSeconds(0.05f);

        currentBullet.gameObject.SetActive(true);

        currentBullet.transform.position = playerState.transformShowBullet.position;
        currentBullet.hitman = playerState.controller;
        currentBullet.ResetState();

    }
    IEnumerator AttackPunch()
    {
        yield return new WaitForSeconds(0.2f);
        RaycastHit2D[] hits;
        Vector2 currentchar = new Vector2(transform.position.x, playerState.transformShowBullet.position.y);
        hits = Physics2D.LinecastAll(currentchar, currentchar + new Vector2(playerState.facingRight ? 0.34f : -0.34f, 0f));
        if (hits.Length > 0)
        {
            foreach (RaycastHit2D perhit in hits)
            {
                if ((perhit.collider.transform.root.CompareTag(Tags.enemy) || Tags.enemy.CompareTo(perhit.collider.tag) == 0) && playerState.isPlayer)
                {
                    Enemy hitman = GameController.Instance.poolController.GetEnemyFromInstanceByName(perhit.collider.name);
                    if (hitman != null)
                    {
                        hitman.Injured(1);
                    }
                    else
                    {
                        GameController.Instance.aiController.TakeDamagePunch();
                        continue;
                    }
                }
                else if ((Tags.player == perhit.collider.transform.root.tag) && !playerState.isPlayer)
                {
                    if (playerState.isPlayer == false)
                    {
                        GameController.Instance.playerController.TakeDamagePunch();
                    }
                }
            }
        }
    }
    #endregion

    public void TakeDamage()
    {
        //rigbd.velocity = new Vector2(0f, rigbd.velocity.y);
        if (!playerState.jumpping && playerState.grounded)
        {

            rigbd.velocity = new Vector2(0f, rigbd.velocity.y);
            float force = playerState.facingRight ? -30f : 30f;
            rigbd.AddRelativeForce(new Vector2(force, 80));
        }
        else
        {
            playerState.canControl = true;
        }
    }

    public void Die()
    {
        rigbd.gravityScale = 0;
        rigbd.isKinematic = true;
    }



    //IEnumerator WaitStartJump(float time)
    //{
    //    playerState.jumpping = true;
    //    playerState.bussy = true;
    //    anim.SetBool(hashID.jumppingBool, true);
    //    yield return new WaitForSeconds(time);
    //    rigbd.AddForce(new Vector2(0f, 115f));
    //    yield return new WaitForSeconds(time / 2);
    //    anim.SetBool(hashID.jumppingBool, false);
    //    playerState.jumpping = false;
    //    playerState.bussy = false;
    //}

    //IEnumerator TakeDamage(bool CanControl)
    //{
    //    menuControl.ShowHealth(playerState.health);
    //    playerState.canControl = CanControl;
    //    if (playerState.health > 0)
    //    {
    //        rigbd.velocity = new Vector2(0f, rigbd.velocity.y);
    //        if (!playerState.jumpping && playerState.grounded)
    //        {
    //            float force = playerState.facingRight ? -30f : 30f;
    //            rigbd.AddRelativeForce(new Vector2(force, 80));
    //        }
    //        playerState.takeDamage = true;
    //        playerState.immortal = true;
    //        yield return new WaitForSeconds(0.75f);
    //        playerState.canControl = true;
    //        playerState.takeDamage = false;
    //        yield return new WaitForSeconds(0.75f);
    //        playerState.immortal = false;
    //    }
    //    else
    //    {
    //        anim.SetBool(hashID.isDieBool, true);
    //        anim.SetTrigger(hashID.isDieTrigger);
    //        rigbd.gravityScale = 0;
    //        rigbd.isKinematic = true;
    //        enabled = false;
    //        playerState.canControl = false;
    //    }
    //}
    //	private void CheckHealth()
    //	{
    //		menuControl.ShowHealth(playerState.health);
    //		if (playerState.health==0) 
    //		{
    //			anim.SetBool(hashID.isDieBool,true);
    //			anim.SetTrigger(hashID.isDieTrigger);
    //			rigbd.gravityScale =0;
    //			rigbd.isKinematic=true;
    //			enabled=false;
    //			playerState.canControl = false;
    //		}
    //	}

    //IEnumerator OnWall()
    //{
    //    playerState.canJumpOnWall = false;
    //    rigbd.velocity = new Vector2(0f, 0f);
    //    rigbd.gravityScale = 0;
    //    yield return new WaitForSeconds(0.25f);
    //    anim.SetBool(hashID.jumpOnWallBool, false);
    //    rigbd.AddForce(new Vector2(30f * (playerState.facingRight ? -1 : 1), 95f));
    //    Flip();
    //    rigbd.gravityScale = 0.5f;
    //}

}