﻿using UnityEngine;
using System.Collections;

public class PlayerState
{

    public bool facingRight = true;
    public float speed = 0.75f;
    public bool immortal = false;
    public bool canControl = true;
    public bool grounded = true;
    public bool canJumpOnWall = false;
    public bool bussy = false;
    public bool jumpOnWall = false;
    public bool jumpping = false;
    public bool sitting = false;

    public bool isPlayer = true;

    public CharacterController controller;

    //State info
    public bool isDead = false;
    //State Physics
    public Transform transformShowBullet;
    //State Animation
    public AnimationState currentAnimation;
    //info
    public float yVelocity;
    public float moveValue;
    public int health = 6;
    public int bulletNumber = 0;
    //public int payBullet = 0;
    public PLAYER_STATE currentState = PLAYER_STATE.NORMAL;
    public WEAPON_STATE currentWeapon = WEAPON_STATE.PUNCH;

    public int Maxhealth = 6;
}

public enum AnimationState
{
    Bussy = 0,
    Jumping = 1,
    Sitting = 2,
    Nothing = 3

}