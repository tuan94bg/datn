﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : CharacterController
{
    PlayerController player;

    public Dictionary<ActionType, RatePointAction> rateAction = new Dictionary<ActionType, RatePointAction>();
    public Dictionary<ActionType, List<ActionType>> rateActionCannot = new Dictionary<ActionType, List<ActionType>>();
    public int difficulLevel = 1;

    private Action playerAction;

    private float distance;
    private bool isPlayerInRight;
    private int maxpoint;
    private float coutTimetoChangeColor = 3f;
    private ActionType type;

    public void InitPoint()
    {
        // POINT IF PLAYER IDLE
        RatePointAction forIdle = new RatePointAction();
        forIdle.AddPointAction(ActionType.IDLE, 0);
        forIdle.AddPointAction(ActionType.MOVE_TO_OTHER, 500);
        forIdle.AddPointAction(ActionType.MOVE_AWAY, 100);
        forIdle.AddPointAction(ActionType.ATTACK, 500);
        forIdle.AddPointAction(ActionType.SIT, 50);
        forIdle.AddPointAction(ActionType.JUMP_TO_OTHER, 100);
        forIdle.AddPointAction(ActionType.JUMP_AWAY, 100);
        forIdle.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.IDLE, forIdle);

        // POINT IF PLAYER MOVE TO AI
        RatePointAction forMoveTo = new RatePointAction();
        forMoveTo.AddPointAction(ActionType.IDLE, 300);
        forMoveTo.AddPointAction(ActionType.MOVE_TO_OTHER, 500);
        forMoveTo.AddPointAction(ActionType.MOVE_AWAY, 100);
        forMoveTo.AddPointAction(ActionType.ATTACK, 500);
        forMoveTo.AddPointAction(ActionType.SIT, 100);
        forMoveTo.AddPointAction(ActionType.JUMP_TO_OTHER, 100);
        forMoveTo.AddPointAction(ActionType.JUMP_AWAY, 50);
        forMoveTo.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.MOVE_TO_OTHER, forMoveTo);

        // POINT IF PLAYER MOVE AWAY
        RatePointAction forMoveAway = new RatePointAction();
        forMoveAway.AddPointAction(ActionType.IDLE, 100);
        forMoveAway.AddPointAction(ActionType.MOVE_TO_OTHER, 500);
        forMoveAway.AddPointAction(ActionType.MOVE_AWAY, 100);
        forMoveAway.AddPointAction(ActionType.ATTACK, 400);
        forMoveAway.AddPointAction(ActionType.SIT, 100);
        forMoveAway.AddPointAction(ActionType.JUMP_TO_OTHER, 150);
        forMoveAway.AddPointAction(ActionType.JUMP_AWAY, 50);
        forMoveAway.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.MOVE_AWAY, forMoveAway);

        // POINT IF PLAYER ATTACK
        RatePointAction forAttack = new RatePointAction();
        forAttack.AddPointAction(ActionType.IDLE, 100);
        forAttack.AddPointAction(ActionType.MOVE_TO_OTHER, 100);
        forAttack.AddPointAction(ActionType.MOVE_AWAY, 200);
        forAttack.AddPointAction(ActionType.ATTACK, 200);
        forAttack.AddPointAction(ActionType.SIT, 350);
        forAttack.AddPointAction(ActionType.JUMP_TO_OTHER, 150);
        forAttack.AddPointAction(ActionType.JUMP_AWAY, 100);
        forAttack.AddPointAction(ActionType.JUMP_IDLE, 100);
        rateAction.Add(ActionType.ATTACK, forAttack);

        // POINT IF PLAYER SIT
        RatePointAction forSit = new RatePointAction();
        forSit.AddPointAction(ActionType.IDLE, 0);
        forSit.AddPointAction(ActionType.MOVE_TO_OTHER, 500);
        forSit.AddPointAction(ActionType.MOVE_AWAY, 100);
        forSit.AddPointAction(ActionType.ATTACK, 300);
        forSit.AddPointAction(ActionType.SIT, 100);
        forSit.AddPointAction(ActionType.JUMP_TO_OTHER, 150);
        forSit.AddPointAction(ActionType.JUMP_AWAY, 50);
        forSit.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.SIT, forSit);

        // POINT IF PLAYER Jum to AI
        RatePointAction forJumTo = new RatePointAction();
        forJumTo.AddPointAction(ActionType.IDLE, 150);
        forJumTo.AddPointAction(ActionType.MOVE_TO_OTHER, 350);
        forJumTo.AddPointAction(ActionType.MOVE_AWAY, 150);
        forJumTo.AddPointAction(ActionType.ATTACK, 500);
        forJumTo.AddPointAction(ActionType.SIT, 200);
        forJumTo.AddPointAction(ActionType.JUMP_TO_OTHER, 300);
        forJumTo.AddPointAction(ActionType.JUMP_AWAY, 150);
        forJumTo.AddPointAction(ActionType.JUMP_IDLE, 150);
        rateAction.Add(ActionType.JUMP_TO_OTHER, forJumTo);

        // POINT IF PLAYER Jum Away
        RatePointAction forJumAway = new RatePointAction();
        forJumAway.AddPointAction(ActionType.IDLE, 100);
        forJumAway.AddPointAction(ActionType.MOVE_TO_OTHER, 600);
        forJumAway.AddPointAction(ActionType.MOVE_AWAY, 100);
        forJumAway.AddPointAction(ActionType.ATTACK, 400);
        forJumAway.AddPointAction(ActionType.SIT, 100);
        forJumAway.AddPointAction(ActionType.JUMP_TO_OTHER, 150);
        forJumAway.AddPointAction(ActionType.JUMP_AWAY, 50);
        forJumAway.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.JUMP_AWAY, forJumAway);

        // POINT IF PLAYER Jum Idle
        RatePointAction forJumIdle = new RatePointAction();
        forJumIdle.AddPointAction(ActionType.IDLE, 50);
        forJumIdle.AddPointAction(ActionType.MOVE_TO_OTHER, 500);
        forJumIdle.AddPointAction(ActionType.MOVE_AWAY, 100);
        forJumIdle.AddPointAction(ActionType.ATTACK, 500);
        forJumIdle.AddPointAction(ActionType.SIT, 50);
        forJumIdle.AddPointAction(ActionType.JUMP_TO_OTHER, 100);
        forJumIdle.AddPointAction(ActionType.JUMP_AWAY, 50);
        forJumIdle.AddPointAction(ActionType.JUMP_IDLE, 50);
        rateAction.Add(ActionType.JUMP_IDLE, forJumIdle);
    }

    void Start()
    {
        gameObject.tag = Tags.enemy;
        playerState.isPlayer = false;
        playerState.bulletNumber = 200;
        player = GameController.Instance.playerController;
        GameController.Instance.aiController = this;
        InitPoint();
        foreach (KeyValuePair<ActionType, RatePointAction> element in rateAction)
        {
            //Debug.Log("point" + element.Value);
        }
        playerAction = new Action();
        StartCoroutine(GetAction());
        // ChangeWeaponState(WEAPON_STATE.BOMERANG);
    }
    void Update()
    {
        base.UpdateAnimation();
        if (!playerState.canControl)
        {
            coutTimetoChangeColor = 2f;
            return;
        }
        else if (coutTimetoChangeColor>0)
        {
            coutTimetoChangeColor -= Time.deltaTime;
            if (coutTimetoChangeColor<= 0)
            {
                playerAnimations.ChangeColorToRedBatMan();
            }
        }
        if (playerState.sitting && !playerState.bussy)
        {
            SitAction();
        }
        else
        {
            EndSitAction();
        }

        if (playerState.jumpOnWall)
        {
            if (playerState.grounded) playerState.jumpOnWall = false;
            //rigbd.AddForce(new Vector2(move * Time.deltaTime * 30, 0));
        }
        else
        {
            if (playerState.currentAnimation == AnimationState.Bussy || playerState.bussy)
            {
                playerPhysics.StopMoveX();
            }
            else
            {
                playerPhysics.Move();
                if (Mathf.Abs(playerState.moveValue) == 1f)
                {
                    EndSitAction();
                }
            }
        }
        playerAnimations.SetFloatSpeed(playerState.moveValue);
        if (((playerState.moveValue > 0.2f && !playerState.facingRight) || (playerState.moveValue < -0.2f && playerState.facingRight)) && !playerState.jumpOnWall)
        {
            playerPhysics.Flip();
        }
    }

    IEnumerator GetAction()
    {
        while (true)
        {
            ChooseAction();
            yield return new WaitForSeconds(0.5f);
        }
    }
    public void ChooseAction()
    {
        //type == playerAction.playerAction;

        distance = player.transform.position.x - transform.position.x;

        Debug.Log(distance);

        if (distance > 0)
        {
            isPlayerInRight = true;
        }
        else
        {
            isPlayerInRight = false;
        }

        float distanceValue = Mathf.Abs(distance);

        if (distanceValue > 1f)
        {
            NextWeapon(WEAPON_STATE.BOMERANG);
        }
        else if (distanceValue > 0.35f)
        {
            NextWeapon(WEAPON_STATE.PUNCH);
        }
        else
        {
            NextWeapon(WEAPON_STATE.SHURIKEN);
        }

        ActionType resultType = ActionType.IDLE;
        int randomPoint = UnityEngine.Random.Range(0, rateAction[type].GetAllPoint());
        foreach (KeyValuePair<ActionType, int> element in rateAction[type].rateAction)
        {
            if (randomPoint < element.Value)
            {
                resultType = element.Key;
                break;
            }
            else
            {
                randomPoint -= element.Value;
            }
        }
        DoAction(resultType);
    }
    private void DoAction(ActionType resultType)
    {
        //resultType = ActionType.ATTACK;

        EndSitAction();
        SetMoveVelocityValue(0);
        if (!playerState.canControl)
        {
            return;
        }
        switch (resultType)
        {
            case ActionType.IDLE:
                SetMoveVelocityValue(0f);
                break;
            case ActionType.MOVE_TO_OTHER:
                if (isPlayerInRight)
                {
                    SetMoveVelocityValue(1);
                }
                else
                {
                    SetMoveVelocityValue(-1);
                }
                break;
            case ActionType.MOVE_AWAY:
                if (isPlayerInRight)
                {
                    SetMoveVelocityValue(-1);
                }
                else
                {
                    SetMoveVelocityValue(1);
                }
                break;
            case ActionType.JUMP_TO_OTHER:
                if (isPlayerInRight)
                {
                    SetMoveVelocityValue(1);
                }
                else
                {
                    SetMoveVelocityValue(-1);
                }
                Jump();
                break;
            case ActionType.JUMP_IDLE:
                if ((isPlayerInRight && !playerState.facingRight) || (!isPlayerInRight && playerState.facingRight))
                {
                    playerPhysics.Flip();
                }
                Jump();
                break;
            case ActionType.JUMP_AWAY:
                if (isPlayerInRight)
                {
                    SetMoveVelocityValue(-1);
                }
                else
                {
                    SetMoveVelocityValue(1);
                }
                Jump();
                break;
            case ActionType.ATTACK:
                //Debug.Log("isPlayerInRight " + isPlayerInRight);
                //Debug.Log("playerState.facingRight " + playerState.facingRight);
                if ((isPlayerInRight && !playerState.facingRight) || (!isPlayerInRight && playerState.facingRight))
                {
                    playerPhysics.Flip();
                    Debug.Log("Da quay");
                }
                //NextWeapon(WEAPON_STATE.PUNCH);
                Attack();
                ChooseAction();
                break;
            case ActionType.SIT:
                if ((isPlayerInRight && !playerState.facingRight) || (!isPlayerInRight && playerState.facingRight))
                {
                    playerPhysics.Flip();
                }
                Sit();
                break;
        }
    }

    public void SetMoveVelocityValue(float x)
    {
        return;
    }

    public int CaculatorPoint(int k, ActionType type, bool isAI)
    {
        RatePointAction rate = rateAction[type];
        //ActionType typeChoose = ActionType.IDLE;
        if (isAI)
        {
            if (k == 0)
            {
                return rate.rateAction[rate.GetTypeMax()];
            }
            else
            {
                return rate.rateAction[rate.GetTypeMax()] + CaculatorPoint(k - 1, rate.GetTypeMax(), !isAI);
            }
        }
        else
        {
            if (k == 0)
            {
                return -rate.rateAction[rate.GetTypeMax()];
            }
            else
            {
                return -rate.rateAction[rate.GetTypeMax()] + CaculatorPoint(k - 1, rate.GetTypeMax(), !isAI);
            }
        }
    }

    public override void Die()
    {
        GameController.Instance.LevelComplete();
    }
}
