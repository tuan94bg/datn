﻿using UnityEngine;
using System.Collections;

public class JSONSerilization :MonoBehaviour
{
	public ListMapCollider GetColliderFromJSON(string text)
	{
		ListMapCollider listCollder;
		listCollder = JsonUtility.FromJson<ListMapCollider>(GetTextFromFile(text));
		return listCollder;
	}

	public string GetTextFromFile(string path)
	{
		TextAsset json = Resources.Load(path) as TextAsset;
		string content = json.text;
		return content;
	}


}
