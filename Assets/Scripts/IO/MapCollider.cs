﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class MapCollider
{
	public Vector3 Offset;
	public Vector3 Size;
}

[Serializable]
public class ListMapCollider
{
	public List<MapCollider> list = new List<MapCollider>();
}

