﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;

public class GameInit : MonoBehaviour
{

    public Image[] progress;
    public int currentLoading = 0;

    public static Sprite mapImageSpriteData;
    public static string mapColliderData;
    public static string mapImageUrl;
    public static string mapColliderUrl;
    public static bool isMapOnBuildPack = false;
    public static bool isRandomEnemy = true;
    public static int levelIndex = 1;

    private float timer = 2f;
    private AsyncOperation async = null; // When assigned, load is in progress.
    private IEnumerator LoadALevel(string levelName)
    {
        async = SceneManager.LoadSceneAsync(levelName);
        yield return async;
    }

    void Start()
    {
        string stageName = "Stage" + levelIndex.ToString();
        if (!isMapOnBuildPack)
        {
            mapColliderUrl = Path.Combine(Application.persistentDataPath, stageName + ".txt");
            mapImageUrl = Path.Combine(Application.persistentDataPath, stageName + ".png");
            StartCoroutine(DownLoadMap());
            //byte[] dataImg = File.ReadAllBytes(mapColliderUrl);
            //Texture2D textureImg = new Texture2D(64, 64, TextureFormat.ARGB32, false);
            //textureImg.LoadImage(dataImg);
            //mapImageSpriteData = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0, 0));


            //byte[] dataCol = File.ReadAllBytes(mapColliderUrl);
            //mapColliderData = 
            //StartCoroutine(LoadALevel("Stage"));
            //mapColliderData = 
            //Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
            //texture.LoadImage(dataCol);

        }
        else
        {
            mapColliderData = ((TextAsset)Resources.Load(string.Format("MapData/{0}/{1}", stageName, stageName)) as TextAsset).text;
            Texture2D textureImg = (Texture2D)Resources.Load(string.Format("MapData/{0}/background", stageName)) as Texture2D;
            mapImageSpriteData = Sprite.Create(textureImg, new Rect(0, 0, textureImg.width, textureImg.height), new Vector2(0, 0));
            StartCoroutine(LoadALevel("Stage"));
        }
    }

    IEnumerator DownLoadMap()
    {
        WWW mapImage = new WWW(mapImageUrl);
        yield return mapImage;
        mapImageSpriteData = Sprite.Create(mapImage.texture, new Rect(0, 0, mapImage.texture.width, mapImage.texture.height), new Vector2(0, 0));
        WWW mapColliderDataWWW = new WWW(mapColliderUrl);
        yield return mapColliderDataWWW;
        mapColliderData = mapColliderDataWWW.text;
        StartCoroutine(LoadALevel("Stage"));
        //Application.LoadLevel("Stage");
    }


    void FixedUpdate()
    {
        if (async != null)
        {
            int nextProgress =(int)(async.progress * 20f);
            if (nextProgress > currentLoading)
            {
             //   int nextProgress = 
                for (int i = currentLoading; i < nextProgress; i++)
                {
                    progress[i].enabled = true;
                }
                currentLoading = nextProgress;
            }
        }
    }
}
